﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyMvc4Project.Dal
{
    public interface IDal<T>
    {
        T GetById(string id);//按id获取表中的一行记录

        IEnumerable<T> GetByClause(string clause);//按条件子句获取表中的记录

        IEnumerable<T> GetAll();//获取表中的所有记录

        IEnumerable<T> GetByPage(int start, int count, string clause, int total);//按页获取

        void Save(T bean);//插入

        void Update(T bean);//更新

        void Delete(string id);//删除
    }
}
