﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using MyMvc4Project.Models;
using MyMvc4Project.Dal.Views;

namespace MyMvc4Project.Dal
{
    public class BootStrapper
    {
        public static void ConfigureAutoMapper()
        {
            AutoMapper.Mapper.CreateMap<Article, ArticleView>();
            AutoMapper.Mapper.CreateMap<Lable, LableView>();
            AutoMapper.Mapper.CreateMap<User, UserView>();
            AutoMapper.Mapper.CreateMap<Comment, CommentDto>();
        }
    }
}
