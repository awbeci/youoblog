﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyMvc4Project.Models;
using MyMvc4Project.Service;
using NHibernate;
using NHibernate.Criterion;
using MyMvc4Project.Dal.Mapper;
using MyMvc4Project.Dal.Views;

namespace MyMvc4Project.Dal.Implementation
{
    public class CommentDal : IDal<Comment>
    {
        readonly ISession _session = SessionHelper.GetCurrentSession();

        public Comment GetById(string id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Comment> GetByClause(string clause)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Comment> GetAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Comment> GetByPage(int start, int count, string clause, int total)
        {
            throw new NotImplementedException();
        }

        public void Save(Comment bean)
        {
            try
            {
                _session.Save(bean);
                _session.Flush();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(Comment bean)
        {
            throw new NotImplementedException();
        }

        public void Delete(string id)
        {
            throw new NotImplementedException();
        }

        public IList<Comment> FindByComment(string id)
        {
            try
            {
                var list = _session.CreateCriteria<Comment>()
                    .AddOrder(Order.Desc("CommentTime"))
                                   .CreateCriteria("Article")
                                   .Add(Restrictions.Eq("Id", id))

                                   .List<Comment>();
                return list;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
