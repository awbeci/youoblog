﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyMvc4Project.Models;
using MyMvc4Project.Service;
using NHibernate;
using NHibernate.Criterion;
using MyMvc4Project.Dal.Mapper;
using MyMvc4Project.Dal.Views;

namespace MyMvc4Project.Dal.Implementation
{
    public class UserDal : IDal<UserView>
    {
        readonly ISession _session = SessionHelper.GetCurrentSession();

        public User GetByUser(string id)
        {
            var user = (User)_session.Load(typeof(User), id);
            return user;
        }

        public UserView GetById(string id)
        {
            throw new NotImplementedException();
        }

        public User GetByName(string name)
        {
            try
            {
                var data = _session.CreateCriteria<User>()
                                   .Add(Restrictions.Eq("Name", name))
                                   .List<User>().FirstOrDefault();
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<UserView> GetByClause(string clause)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<UserView> GetAll()
        {
            try
            {
                var query = _session.CreateCriteria<User>();
                var data = query.List<User>();
                return UserMapper.GetUserView(data);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IList<User> GetAll1()
        {
            try
            {
                var query = _session.CreateCriteria<User>();
                var data = query.List<User>();
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<UserView> GetByPage(int start, int count, string clause, int total)
        {
            throw new NotImplementedException();
        }

        public void Save(UserView bean)
        {
            throw new NotImplementedException();
        }

        public void Save(User bean)
        {
            try
            {
                _session.Save(bean);
                _session.Flush();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(UserView bean)
        {
            throw new NotImplementedException();
        }

        public void Update(User bean)
        {
            try
            {
                var user = (User)_session.Load(typeof(User), bean.Id);
                user.PostTitle = bean.PostTitle;
                user.Avatar = bean.Avatar;
                user.Department = bean.Department;
                _session.Update(user);
                _session.Flush();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(string id)
        {
            try
            {
                var user = (User)_session.Load(typeof(User), id);
                _session.Delete(user);
                _session.Flush();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public User GetUserBy(string name, string pass)
        {
            try
            {
                var query = _session.CreateCriteria<User>()
                    .Add(Restrictions.Eq("Name", name))
                    .Add(Restrictions.Eq("Password", pass));

                var data = query.List<User>().SingleOrDefault();
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public User GetUserBy(string name)
        {
            try
            {
                var query = _session.CreateCriteria<User>()
                                    .Add(Restrictions.Eq("Name", name));

                var data = query.List<User>().SingleOrDefault();
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<User> MyGetAllByUserName(int start, int limit, ref int count)
        {
            try
            {
                var query = _session.CreateCriteria<User>();

                count = query.List<User>().Count;

                var data = query.SetFirstResult(start)
                                .SetMaxResults(limit)
                                .AddOrder(Order.Asc("CreateTime"))
                                .List<User>();
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
