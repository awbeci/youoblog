﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyMvc4Project.Models;
using MyMvc4Project.Service;
using NHibernate;
using NHibernate.Criterion;

namespace MyMvc4Project.Dal.Implementation
{
    public class BlogDal : IDal<Blog>
    {
        readonly ISession _session = SessionHelper.GetCurrentSession();
        public Blog GetById(string id)
        {
            try
            {
                return (Blog) _session.Load(typeof (Blog), id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Blog> GetByClause(string clause)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Blog> GetAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Blog> GetByPage(int start, int count, string clause, int total)
        {
            throw new NotImplementedException();
        }

        public void Save(Blog bean)
        {
            try
            {
                _session.Save(bean);
                _session.Flush();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(Blog bean)
        {
            try
            {
                var blog = (Blog) _session.Load(typeof (Blog), bean.Id);
                blog.Id = bean.Id;
                blog.SmallTitle = bean.SmallTitle;
                blog.BigTitle = bean.BigTitle;
                blog.BlogCss = bean.BlogCss;
                _session.Update(blog);
                _session.Flush();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(string id)
        {
            throw new NotImplementedException();
        }
    }
}
