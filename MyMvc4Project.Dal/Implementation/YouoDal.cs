﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyMvc4Project.Models;
using MyMvc4Project.Service;
using NHibernate;
using NHibernate.Criterion;
using MyMvc4Project.Dal.Mapper;
using MyMvc4Project.Dal.Views;

namespace MyMvc4Project.Dal.Implementation
{
    public class YouoDal : IDal<Youo>
    {
        readonly ISession _session = SessionHelper.GetCurrentSession();
        public Youo GetById(string id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Youo> GetByClause(string clause)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Youo> GetAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Youo> GetByPage(int start, int count, string clause, int total)
        {
            throw new NotImplementedException();
        }

        public void Save(Youo bean)
        {
            try
            {
                _session.Save(bean);
                _session.Flush();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(Youo bean)
        {
            try
            {
                var youo = (Youo) _session.Load(typeof (Youo), bean.Id);
                youo.Contents = bean.Contents;
                _session.Update(youo);
                _session.Flush();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(string id)
        {
            try
            {
                var youo = (Youo) _session.Load(typeof (Youo), id);
                _session.Delete(youo);
                _session.Flush();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Youo MyGetFirst()
        {
            try
            {
                var data = _session.CreateCriteria<Youo>()
                                   .AddOrder(new Order("CreateTime", false))
                                   .List<Youo>().FirstOrDefault();
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Youo> MyGetAll(int start, int limit, ref int count)
        {
            try
            {
                var query = _session.CreateCriteria<Youo>();

                count = query.List<Youo>().Count;

                var data = query.SetFirstResult(start)
                                .SetMaxResults(limit)
                                .AddOrder(Order.Asc("CreateTime"))
                                .List<Youo>();
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
