﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyMvc4Project.Models;
using MyMvc4Project.Service;
using NHibernate;
using NHibernate.Criterion;
using MyMvc4Project.Dal.Mapper;
using MyMvc4Project.Dal.Views;

namespace MyMvc4Project.Dal.Implementation
{
    public class ArticleDal : IDal<ArticleView>
    {
        readonly ISession _session = SessionHelper.GetCurrentSession();

        public ArticleView GetById(string id)
        {
            throw new NotImplementedException();
        }


        public Article GetById(string id, string name)
        {
            try
            {
                var list = _session.CreateCriteria<Article>()
                    .Add(Restrictions.Eq("Id", id))
                     .CreateCriteria("User")
                    .Add(Restrictions.Eq("Name", name))
                    .List<Article>().SingleOrDefault();
                return list;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Article> GetById2(string id, string name)
        {
            try
            {
                var list = _session.CreateCriteria<Article>()
                    .Add(Restrictions.Eq("Id", id))
                    .CreateCriteria("User")
                    .Add(Restrictions.Eq("Name", name))
                    .List<Article>();
                return list;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<ArticleView> GetByClause(string clause)
        {
            try
            {
                var list = _session.CreateCriteria<Article>()
                    .CreateCriteria("User")
                    .Add(Restrictions.Eq("Name", clause))
                    .List<Article>();
                return ArticleMapper.GetArticleView(list);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Article> GetByClause(string name, string lbl)
        {
            try
            {
                var list = _session.CreateSQLQuery(@"SELECT a.Id,a.UserId,a.Title,a.ClickCount,a.Contents,a.CreateTime,a.UpdateTime
                                                     FROM YouoBlog.Article a
                                                     inner join YouoBlog.Users u on u.Id=a.UserId
                                                     inner join YouoBlog.ArticleLable al on al.ArticleId = a.Id
                                                     inner join YouoBlog.Lable l on l.Id = al.LableId
                                                     where l.Name=:lblname and u.Name=:uname;")
                                      .AddEntity("Article", typeof(Article))
                                      .SetString("lblname", lbl)
                                      .SetString("uname", name)


                    .List<Article>();
                return list;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Article> GetByClause(string name, DateTime dateTime1, DateTime dateTime2)
        {
            try
            {
                var list = _session.CreateSQLQuery(@"SELECT a.Id,a.UserId,a.Title,a.ClickCount,a.Contents,a.CreateTime,a.UpdateTime
                                                     FROM YouoBlog.Article a
                                                     inner join YouoBlog.Users u on u.Id=a.UserId
                                                     where u.Name=:uname and a.CreateTime >=:dt1  and a.CreateTime <:dt2;")
                                      .AddEntity("Article", typeof(Article))
                                      .SetDateTime("dt1", dateTime1)
                                       .SetDateTime("dt2", dateTime2)
                                      .SetString("uname", name)
                                      .List<Article>()
                                      .OrderByDescending(s => s.CreateTime);
                return list;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<object[]> GetBySql(string name)
        {
            try
            {
                var list = _session.CreateSQLQuery(@"SELECT date_format(a.CreateTime,'%Y年%c月') date,count(a.CreateTime) count,date_format(a.CreateTime,'%Y-%m')
                                                     FROM YouoBlog.Article a inner join YouoBlog.Users u on u.Id=a.UserId
                                                     where u.Name=:name
                                                     group by date_format(a.CreateTime,'%Y-%m')
                                                     order by a.CreateTime desc")
                                  .SetString("name", name)
                                  .List<object[]>();
                return list;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<ArticleView> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="start"></param>
        /// <param name="limit"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public IEnumerable<Article> MyGetAll(string name, int start, int limit, ref int count)
        {
            try
            {
                var query = _session.CreateCriteria<Article>()
                                    .CreateCriteria("User")
                                    .Add(Restrictions.Eq("Name", name));
                count = query.List<Article>().Count;
                var data = query.SetFirstResult((start - 1) * limit)
                                .SetMaxResults(limit)
                                .AddOrder(Order.Desc("CreateTime"))
                                .List<Article>()
                                .OrderByDescending(s => s.CreateTime);
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="start"></param>
        /// <param name="limit"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public IEnumerable<Article> MyGetAll(int start, int limit, ref int count)
        {
            try
            {
                var query = _session.CreateCriteria<Article>();
                count = query.List<Article>().Count;
                var data = query.SetFirstResult((start - 1) * limit)
                                .SetMaxResults(limit)
                                .AddOrder(new Order("CreateTime", false))
                                .List<Article>();
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="start"></param>
        /// <param name="limit"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public IEnumerable<ArticleView> MyGetAll2(string username, int start, int limit, ref int count)
        {
            try
            {
                var query = _session.CreateCriteria<Article>()
                     .CreateCriteria("User")
                    .Add(Restrictions.Eq("Name", username));
                count = query.List<Article>().Count;
                var data = query.SetFirstResult(start)
                                .SetMaxResults(limit)
                                .AddOrder(new Order("CreateTime", false))
                                .List<Article>();
                return ArticleMapper.GetArticleView(data);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<object> MyGetAll3(string username, int start, int limit, ref int count)
        {

            try
            {
                var query = _session.CreateCriteria<Article>()
                     .CreateCriteria("User")
                    .Add(Restrictions.Eq("Name", username));
                count = query.List<Article>().Count;
                var data = query.SetFirstResult(start)
                                .SetMaxResults(limit)
                                .List<Article>().OrderByDescending(s => s.UpdateTime);
                return from article in data
                       select new
                           {
                               article.Id,
                               article.Title,
                               article.ClickCount,
                               article.Comments.Count
                           };
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<ArticleView> GetByPage(int start, int count, string clause, int total)
        {
            throw new NotImplementedException();
        }

        public void Save(ArticleView bean)
        {
            throw new NotImplementedException();
        }

        public void Save(Article bean)
        {
            try
            {
                _session.Save(bean);
                _session.Flush();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void Update(ArticleView bean)
        {
            throw new NotImplementedException();
        }

        public void UpdateClickCount(Article bean)
        {
            var article = (Article)_session.Load(typeof(Article), bean.Id);
            article.ClickCount++;
            try
            {
                _session.Update(article);
                _session.Flush();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(Article bean)
        {
            //这行代码非常的重要，对于nhibernate多对多级联更新
            var article = (Article)_session.Load(typeof(Article), bean.Id);

            var lblList = bean.Lables.Select(
                lable => (Lable)_session.Load(typeof(Lable), lable.Id)
                ).ToList();

            article.User = bean.User;
            article.Title = bean.Title;
            article.Contents = bean.Contents;
            article.UpdateTime = DateTime.Now;
            article.Lables = lblList;
            try
            {
                _session.Update(article);
                _session.Flush();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        public void Delete(string id)
        {
            try
            {
                var article = (Article)_session.Load(typeof(Article), id);
                _session.Delete(article);
                _session.Flush();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
