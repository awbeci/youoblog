﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyMvc4Project.Models;
using MyMvc4Project.Service;
using NHibernate;
using NHibernate.Criterion;
using MyMvc4Project.Dal.Mapper;
using MyMvc4Project.Dal.Views;

namespace MyMvc4Project.Dal.Implementation
{
    public class LableDal : IDal<Lable>
    {
        readonly ISession _session = SessionHelper.GetCurrentSession();
        public Lable GetById(string id)
        {
            throw new NotImplementedException();
        }
        public Lable GetByName(string name)
        {
            try
            {
                var data = _session.CreateCriteria<Lable>()
                                  .Add(Restrictions.Eq("Name", name))
                                  .List<Lable>().FirstOrDefault();
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Lable> GetByClause(string clause)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Lable> GetAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Lable> GetByPage(int start, int count, string clause, int total)
        {
            throw new NotImplementedException();
        }

        public void Save(Lable bean)
        {
            try
            {
                _session.Save(bean);
                _session.Flush();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(Lable bean)
        {
            try
            {
                //这行代码非常的重要，对于nhibernate多对多级联更新
                var lable = (Lable)_session.Load(typeof(Lable), bean.Id);
                lable.Name = bean.Name;
                lable.Level = bean.Level;
                lable.Users = bean.Users;
                _session.Update(lable);
                _session.Flush();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void UpdateName(Lable bean)
        {
            try
            {
                //这行代码非常的重要，对于nhibernate多对多级联更新
                var lable = (Lable)_session.Load(typeof(Lable), bean.Id);
                lable.Name = bean.Name;
                lable.Users = bean.Users;
                _session.Update(lable);
                _session.Flush();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void UpdateLevel(Lable bean)
        {
            try
            {
                //这行代码非常的重要，对于nhibernate多对多级联更新
                var lable = (Lable)_session.Load(typeof(Lable), bean.Id);
                lable.Level = bean.Level;
                lable.Users = bean.Users;
                _session.Update(lable);
                _session.Flush();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(string id)
        {
            try
            {
                var lable = (Lable)_session.Load(typeof(Lable), id);
                _session.Delete(lable);
                _session.Flush();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IList<Lable> MyGetAll()
        {
            try
            {
                var list = _session.CreateCriteria<Lable>()
                                   .List<Lable>();
                return list;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<LableView> MyGetAllByUserId(string name)
        {
            try
            {
                var list = _session.CreateQuery("from Lable l left join fetch Users").List();
                //var list = _session.CreateCriteria<Lable>()
                //    .CreateCriteria("Users")
                //    .Add(Restrictions.Eq("Name", name))
                //                   .List<Lable>().Distinct();
                //return LableMapper.GetLableView(list);
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Lable> MyGetAllByUserId2(string name)
        {
            try
            {
                var list = _session.CreateCriteria<Lable>()
                    .CreateCriteria("Users")
                    .Add(Restrictions.Eq("Name", name))
                                   .List<Lable>().Distinct();
                return list;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="articleId"></param>
        /// <returns></returns>
        public IEnumerable<LableView> MyGetAllByUserIdAndArticleId(string username, string articleId)
        {
            try
            {
                var list = _session.CreateCriteria<Lable>()
                    .CreateCriteria("Articles")
                    .Add(Restrictions.Eq("Id", articleId))
                    .CreateCriteria("User")
                    .Add(Restrictions.Eq("Name", username))
                                   .List<Lable>().Distinct();
                return LableMapper.GetLableView(list);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<LableView> MyGetAllByUserName(string username, int start, int limit, ref int count)
        {
            try
            {
                var query = _session.CreateCriteria<Lable>()
                     .CreateCriteria("Users")
                    .Add(Restrictions.Eq("Name", username));

                count = query.List<Lable>().Count;

                var data = query.SetFirstResult(start)
                                .SetMaxResults(limit)
                                .AddOrder(Order.Asc("CreateTime"))
                                .List<Lable>();
                return LableMapper.GetLableView(data);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Lable> GetAllByUserName(string username)
        {
            try
            {
                var data = _session.CreateCriteria<Lable>()
                     .CreateCriteria("Users")
                    .Add(Restrictions.Eq("Name", username))
                    .List<Lable>();
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
