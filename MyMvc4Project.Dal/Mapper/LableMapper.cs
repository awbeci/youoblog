﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using MyMvc4Project.Dal.Views;
using MyMvc4Project.Models;

namespace MyMvc4Project.Dal.Mapper
{
    public class LableMapper
    {
        public static IEnumerable<LableView> GetLableView(IEnumerable<Lable> lables)
        {
            return AutoMapper.Mapper.Map<IEnumerable<Lable>,
                IEnumerable<LableView>>(lables);
        }

        public static IList<LableView> GetLableDto(IList<Lable> lables)
        {
            return AutoMapper.Mapper.Map<IList<Lable>,
                IList<LableView>>(lables);
        }
    }
}
