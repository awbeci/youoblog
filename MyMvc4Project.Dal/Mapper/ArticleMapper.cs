﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using MyMvc4Project.Dal.Views;
using MyMvc4Project.Models;

namespace MyMvc4Project.Dal.Mapper
{
   public static class ArticleMapper
    {
       public static IEnumerable<ArticleView> GetArticleView(IEnumerable<Article> articles)
       {
           return AutoMapper.Mapper.Map<IEnumerable<Article>, IEnumerable<ArticleView>>(articles);
       }

       public static ArticleView GetArticleView(Article article)
       {
           return AutoMapper.Mapper.Map<Article, ArticleView>(article);
       }
    }
}
