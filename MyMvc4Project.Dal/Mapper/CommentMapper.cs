﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using MyMvc4Project.Dal.Views;
using MyMvc4Project.Models;

namespace MyMvc4Project.Dal.Mapper
{
    public class CommentMapper
    {
        public static IEnumerable<CommentDto> GetCommentView(IEnumerable<Comment> comments)
        {
            return AutoMapper.Mapper.Map<IEnumerable<Comment>,
                IEnumerable<CommentDto>>(comments);
        }

        public static IList<CommentDto> GetCommentView(IList<Comment> comments)
        {
            return AutoMapper.Mapper.Map<IList<Comment>,
                IList<CommentDto>>(comments);
        }
    }
}
