﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using MyMvc4Project.Dal.Views;
using MyMvc4Project.Models;

namespace MyMvc4Project.Dal.Mapper
{
    public class UserMapper
    {
        public static IEnumerable<UserView> GetUserView(IEnumerable<User> user)
        {
            return AutoMapper.Mapper.Map<IEnumerable<User>, IEnumerable<UserView>>(user);
        }

        public static UserView GetUserView(User user)
        {
            return AutoMapper.Mapper.Map<User, UserView>(user);
        }
    }
}
