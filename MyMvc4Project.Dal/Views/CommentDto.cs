﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyMvc4Project.Models;

namespace MyMvc4Project.Dal.Views
{
    public class CommentDto
    {
        /// <summary>
        /// id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 文章
        /// </summary>
        public string ArticleId { get; set; }

        /// <summary>
        /// 被评论用户
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// 被评论用户
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 被评论用户
        /// </summary>
        public string UserAvatar { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public string Contents { get; set; }

        /// <summary>
        /// 评论时间
        /// </summary>
        public DateTime? CommentTime { get; set; }
    }
}
