﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyMvc4Project.Dal.Views
{
    public class UserView
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 员工职称
        /// </summary>
        public string PostTitle { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { get; set; }

        public string BlogSmallTitle { get; set; }

        public string BlogBigTitle { get; set; }

        public string BlogBlogCss { get; set; }

        public IList<LableView> Labels { get; set; }
    }
}
