﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyMvc4Project.Models;

namespace MyMvc4Project.Dal.Views
{
    public class ArticleView
    {
        /// <summary>
        /// id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 用户
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// 用户
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 用户职称
        /// </summary>
        public string UserPostTitle { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public string UserAvatar { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public string Contents { get; set; }

        /// <summary>
        /// 点击浏览次数
        /// </summary>
        public int ClickCount { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }

        public IList<LableView> Lables { get; set; }
    }
}
