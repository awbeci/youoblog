﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyMvc4Project.Dal.Views
{
    public class LableView
    {
        /// <summary>
        /// id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 标签名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 显示等级
        /// </summary>
        public int Level { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { get; set; }
        
    }
}
