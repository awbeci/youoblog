/*==============================================================*/
/* Table: t_Article                                             */
/*==============================================================*/
create table t_Article 
(
   Id                   varchar(40)                    not null,
   Title                varchar(100)                   null,
   Content              varchar(30000)                 null,
   CreateTime           datetime                       null,
   Creater              varchar(20)                    null,
   UpdateTime           datetime                       null,
   Updater              varchar(20)                    null,
   UserId               varchar(50)                    null,
   constraint PK_T_ARTICLE primary key clustered (Id)
);


/*==============================================================*/
/* Table: t_Article_Lable                                       */
/*==============================================================*/
create table t_Article_Lable 
(
   Id                   varchar(40)                    not null,
   LableId              varchar(40)                    null,
   ArticleId            varchar(40)                    null,
   CreateTime           datetime                       null,
   Creater              varchar(20)                    null,
   UpdateTime           datetime                       null,
   Updater              varchar(20)                    null,
   constraint PK_T_ARTICLE_LABLE primary key clustered (Id)
);


/*==============================================================*/
/* Table: t_Lable                                               */
/*==============================================================*/
create table t_Lable 
(
   Id                   varchar(40)                    not null,
   Name                 varchar(100)                   null,
   constraint PK_T_LABLE primary key clustered (Id)
);


/*==============================================================*/
/* Table: t_Menu                                                */
/*==============================================================*/
create table t_Menu 
(
   Id                   varchar(40)                    not null,
   Title                varchar(10)                    null,
   Url                  varchar(100)                   null,
   constraint PK_T_MENU primary key clustered (Id)
);



/*==============================================================*/
/* Table: t_Comment                                             */
/*==============================================================*/
create table t_Comment 
(
   Id                   varchar(40)                    not null,
   Content              varchar(1000)                  null,
   UserId               varchar(40)                    null,
   CommentTime          datetime                       null,
   CommenterId          varchar(40)                    null,
   constraint PK_T_COMMENT primary key clustered (Id)
);


/*==============================================================*/
/* Table: t_User                                                */
/*==============================================================*/
create table t_User 
(
   Id                   varchar(40)                    not null,
   Name                 varchar(20)                    null,
   Password             varchar(10)                    null,
   Sex                  binary                         null,
   Age                  numeric                        null,
   UserPhotoUrl         varchar(200)                   null,
   constraint PK_T_USER primary key clustered (Id)
);