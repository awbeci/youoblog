﻿$(function () {
    getAllArticle();
    getAllLable();
    getYouo();
    getXmlDoc();
    getAllUser();
    $('#jumbotron').hide();
    showTip();
    scrollUp();
    setTimeout(showTip2, 1000);
});
var myCurrentPage = 1, myTotalPages = 0;
//------------------------------------------------------------------------------------

function bootStrapPage() {
    var options = {
        currentPage: myCurrentPage,
        totalPages: myTotalPages,
        numberOfPages: 15,
        size: 'small',
        itemContainerClass: function (type, page, current) {
            return (page === current) ? "active" : "pointer-cursor";
        },
        itemTexts: function (type, page, current) {
            switch (type) {
                case "first":
                    return "First";
                case "prev":
                    return "上一页";
                case "next":
                    return "下一页";
                case "last":
                    return "Last";
                case "page":
                    return page;
                default:
                    return null;
            }
        },
        shouldShowPage: function (type, page, current) {
            switch (type) {
                case "first":
                case "last":
                    return false;
                default:
                    return true;
            }
        },
        onPageClicked: function (e, originalEvent, type, page) {
            myCurrentPage = page;
            zwobj.url = Resolve('/GetAllArticle/?currentPage=' + myCurrentPage);
            ajaxData();
        }
    };
    $('#example').bootstrapPaginator(options);
}

function showTip2() {
    if ($('#colspan').hasClass('glyphicon-chevron-up')) {
        $('#colspan').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
    } else {
        $('#colspan').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
    }
    $('#jumbotron').animate({ height: 'toggle' }, 500);
}

function showTip() {
    $('#colspan').click(function () {
        if ($('#colspan').hasClass('glyphicon-chevron-up')) {
            $('#colspan').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        } else {
            $('#colspan').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        }
        $('#jumbotron').animate({
            height: 'toggle'
        }, 100);
    });
}

function getAllArticle() {
    zwobj.url ='/home/GetAllArticle/?currentPage=' + myCurrentPage;
    zwobj.data = {};
    ajaxData();
}

function getAllLable() {
    zwobj.url = '/home/GetAllLable';
    zwobj.data = {};
    ajaxData();
}

function getYouo() {
    zwobj.url = '/home/GetYouo';
    zwobj.data = {};
    ajaxData();
}

function getXmlDoc() {
    zwobj.url = '/home/GetRssXmlDoc';
    ajaxData();
}


function getAllUser() {
    zwobj.url = '/home/GetAllUser';
    ajaxData();
}


function scrollUp() {
    $.scrollUp({
        scrollName: 'scrollUp', // Element ID
        topDistance: '300', // Distance from top before showing element (px)
        topSpeed: 300, // Speed back to top (ms)
        animation: 'fade', // Fade, slide, none
        animationInSpeed: 200, // Animation in speed (ms)
        animationOutSpeed: 200, // Animation out speed (ms)
        scrollText: '', // Text for element
        activeOverlay: false  // Set CSS color to display scrollUp active point, e.g '#00FFFF'
    });
}
//-----------------------------------------------------------------------------------------------

function login() {
    location.href = '/Logins/Login';
}
function quit() {
    zwobj.url = '/home/Quit';
    ajaxData();
}

function regist() {
    location.href = '/logins/Register';
}
//------------------------------------------------------------------------------------------------
function ajax_GetAllArticle(data) {
    $(".content_list tbody").empty();
    var html = "";
    var len = data.Data.length;
    for (var i = 0; i < len; i++) {
        html += '<tr><td scope="row"><div class="clearfix">发布于:&nbsp;<label class="color-lbl">'
             + eval("new " + data.Data[i].CreateTime.split('/')[1]).Format("yyyy-MM-dd HH:mm") + '</label></div>'
             + '<div class="clearfix"><div class="clearfix-left">作者:</div><div class="clearfix-left">&nbsp;'
             + '<img src="' + data.Data[i].UserAvatar + '"alt="author">&nbsp;</div><div class="common-a"><a href="/Blog/Index/' + data.Data[i].UserName + '">'
             + data.Data[i].UserName + '</a></div></div>'
             + '<div class="clearfix">职称:&nbsp;<label class="color-lbl">' + data.Data[i].UserPostTitle + '</label></div>'
             + '<div class="clearfix"><div class="clearfix-left">所属部门:<label class="color-lbl">' + data.Data[i].Department + '</label></div><div class="clearfix-left">&nbsp;'
             //+ '<img src="../../Images/qq.png" />&nbsp;<img src="../../Images/sina.jpg" />&nbsp;<img src="../../Images/weibo.ico" /></div></div>'
             + '</td><td><h3><a href="/Blog/Article?name=' + data.Data[i].UserName + '&id=' + data.Data[i].Id + '">' + data.Data[i].Title + '</a></h3>'
             + '<div class="td-content">' + decodeURI(data.Data[i].Contents).replace(/<[^>].*?>/g, "").substr(0, 130) + '...</div>'
             + '<div class="common-a">Tags:';
        var len2 = data.Data[i].Lables.length;
        for (var j = 0; j < len2; j++) {
            html += '<a href="/Blog/ArticleList?name=' + data.Data[i].UserName + '&para=' + 
                data.Data[i].Lables[j].Name + '&type=lb" rel="tag">' + data.Data[i].Lables[j].Name + '</a>&nbsp;&nbsp;';
        }
        html += '</div></td>';
    }
    $(".content_list tbody").append(html);

    //总页数
    var total = parseInt(data.Other);
    myTotalPages = Math.ceil(total / 20);
    bootStrapPage();
}

function ajax_GetAllLable(data) {
    $(".panel-sign .panel-body").empty();
    var html = "<ul>";
    var len = data.Data.length;
    for (var i = 0; i < len; i++) {
        html += '<li class="level-' + data.Data[i].Level + '"><a href="#">'
            + data.Data[i].Name + '</a></li>';
    }
    $(".panel-sign .panel-body").append(html);
}

function ajax_GetYouo(data) {
    var html = "";
    $(".panel-note .panel-body").empty();
    html += '<p>' + data.FirstData.Contents + '</p>';
    html += '<p>[ ' + eval("new " + data.FirstData.CreateTime.split('/')[1]).Format("yyyy年MM月dd日") + ' ]</p>';
    $(".panel-note .panel-body").append(html);
}


function ajax_GetRssXmlDoc(data) {
    var html = "";
    var len = data.Data.length;
    $(".panel-join .panel-body").empty();
    html += '<ul>';
    for (var i = 0; i < 10; i++) {
        html += '<li><a href="' + data.Data[i].Link + '" title="' + data.Data[i].Title + '" target="_blank">';
        if (data.Data[i].Title.length <= 19) {
            html += data.Data[i].Title + '</a></li>';
        } else {
            html += data.Data[i].Title.substr(0, 17) + '...</a></li>';
        }
    }
    html += '</ul>';
    $(".panel-join .panel-body").append(html);
}

function ajax_GetAllUser(data) {
    var html = "", imghtml = "", nameHtml = "";
    var len = data.Data.length;
    $(".panel-code .panel-body").empty();
    html += '<table border="0" cellpadding="0" cellspacing="3" width="100%"><tbody>';
    for (var i = 0; i < len; i++) {
        if ((i % 4) == 0) {
            html += imghtml + nameHtml;
            imghtml = "", nameHtml = "";
            imghtml += '<tr><td align="center"><a href="/Blog/Index/' + data.Data[i].Name + '" target="_blank"><img align="top" alt="" src="' +
                data.Data[i].Avatar + '" title=""></a></td>';

            nameHtml += '<tr><td align="center" height="30"><a href="/Blog/Index/' + data.Data[i].Name + '" target="_blank">' +
                 data.Data[i].Name + '</a></td>';

        } else {
            imghtml += '<td align="center"><a href="/Blog/Index/' + data.Data[i].Name + '" target="_blank"><img align="top" alt="" src="' +
                data.Data[i].Avatar + '" title=""></a></td>';
            nameHtml += '<td align="center" height="30"><a href="/Blog/Index/' + data.Data[i].Name + '" target="_blank">' +
                 data.Data[i].Name + '</a></td>';
        }
    }
    html += imghtml + nameHtml;
    html += '</tbody></table>';
    $(".panel-code .panel-body").append(html);
}

function ajax_Quit(data) {
    location.href = '/Home';
}