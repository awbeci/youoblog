﻿var keditor;
var koption = {
    allowFileManager: true, //是否可以浏览上传文件
    allowUpload: true, //是否可以上传
    fileManagerJson: '/KindEditor/ProcessRequest', //浏览文件方法
    uploadJson: '/KindEditor/UploadImage'//上传文件方法(注意这两个路径)
};

KindEditor.ready(function (k) {
    keditor = k.create('#content-js', koption);
    prettyPrint();
});

function btn_submit() {
    var lables = [];
    var title = $("#txt_title").val();
    if (title.length == 0 || keditor.isEmpty()) {
        alert("标题或内容为空！");
        return false;
    }
    $("input[name=lable]:checked").each(function () {
        lables.push($(this).val());
    });
    zwobj.url = "/Manage/UpdateArticle";
    zwobj.data = {  
        id:$("#hiddid").val(),
        title: title,
        context: encodeURI(keditor.html()),
        lblId: lables.join(',')
    };
    ajaxData();
    return true;
}

function ajax_AddArticle() {
    location.href = "/BlogManage/ArticleManage/?action=edit";
}