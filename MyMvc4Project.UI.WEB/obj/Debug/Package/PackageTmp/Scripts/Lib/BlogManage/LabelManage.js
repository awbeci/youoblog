﻿$(function () {
    initTable();
    $(".fg-toolbar:first-child").css("display", "none");
    $("#txtLbl").focus();
});

function initTable() {
    var name = $("#name").text();
    var lblName = '';
    var oTable = $('#table-example').dataTable({
        bLengthChange: false,
        bFilter: false, //搜索栏
        bPaginate: false, //翻页功能
        bAutoWidth: true,  //自适应宽度
        bProcessing: false,
        bServerSide: true,
        bDeferRender: true,
        bInfo: false,
        sAjaxSource: "/BlogManage/GetLabelData",
        aoColumns: [
        {
            sTitle: "Id",
            mData: 'Id',
            bVisible: false
        },
        {
            sTitle: "标签名",
            mData: 'Name',
            sClass: "lTitle",
            fnRender: function (oObj) {
                lblName = oObj.aData.Name;
                return '<span style="display:inline;">' + oObj.aData.Name + '</span>';
            }
        }, {
            sTitle: "等级",
            mData: 'Level',
            sClass: "lClass",
            fnRender: function (oObj) {
                switch (oObj.aData.Level) {
                    case 1:
                        return '<select onchange="javascript:change(\'' + oObj.aData.Id + '\',this.value);" class="levelSel"><option selected="selected">1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option></select>'

                    case 2:
                        return '<select onchange="javascript:change(\'' + oObj.aData.Id + '\',this.value);" class="levelSel"><option>1</option><option selected="selected">2</option><option>3</option><option>4</option><option>5</option><option>6</option></select>';

                    case 3:
                        return '<select onchange="javascript:change(\'' + oObj.aData.Id + '\',this.value);" class="levelSel"><option>1</option><option>2</option><option selected="selected">3</option><option>4</option><option>5</option><option>6</option></select>';

                    case 4:
                        return '<select onchange="javascript:change(\'' + oObj.aData.Id + '\',this.value);" class="levelSel"><option>1</option><option>2</option><option>3</option><option selected="selected">4</option><option>5</option><option>6</option></select>';

                    case 5:
                        return '<select onchange="javascript:change(\'' + oObj.aData.Id + '\',this.value);" class="levelSel"><option>1</option><option>2</option><option>3</option><option>4</option><option selected="selected">5</option><option>6</option></select>';

                    case 6:
                        return '<select onchange="javascript:change(\'' + oObj.aData.Id + '\',this.value);" class="levelSel"><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option selected="selected">6</option></select>';

                    default: return '<select onchange="javascript:change(\'' + oObj.aData.Id + '\',this.value);" class="levelSel"><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option></select>';
                }

            }
        }
         , {
             sTitle: "关联文章",
             mData: "Count",
             sClass: "lClass",
             fnRender: function (oObj) {
                 return '<a href="/Blog/ArticleList?name=' + name + '&para=' + lblName + '&type=lb ">' +
                                oObj.aData.Count + '</a>';
             }
         },
        {
            sTitle: "操作",
            mData: null,
            sClass: "lClass",
            fnRender: function (oObj) {
                return '<a href="javascript:void(0)" onclick="javascript:edit(this,\'' + oObj.aData.Id + '\')">编辑</a> | <a href="javascript:void(0)" onclick="del_click(\'' + oObj.aData.Id + '\')" class="editor_remove">删除</a>';
            }
        }],
        oLanguage: {
            sLengthMenu: "每页显示 _MENU_ 条记录",
            sZeroRecords: "抱歉， 没有找到",
            sInfo: "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
            sInfoEmpty: "没有数据",
            sInfoFiltered: "(从 _MAX_ 条数据中检索)",
            sZeroRecords: "没有检索到数据",
            sSearch: "名称:",
            oPaginate: {
                sFirst: "首页",
                sPrevious: "前一页",
                sNext: "后一页",
                sLast: "尾页"
            }
        }
    });
}


function del_click(id) {
    if (confirm("确定要删除数据？")) {
        var data = { id: id };
        $.get("/Manage/DeleteLabel", data, function () {
            RefreshTable('#table-example', '/BlogManage/GetLabelData');
        });
    }
}

function ajax_DeleteArticle() {
    location.href = '/BlogManage/ArticleManage/?action=del';
}
var e_tr = "";
function edit(e, id) {
    cancelEdit();
    e_tr = $(e).parent().parent();

    var val = $(e).parent().parent().children("td:eq(0)").children().html();
    $(e).parent().parent().children("td:eq(0)").children().attr("style", "display:none");
    $(e).parent().parent().children("td:eq(0)").append("<span><input style='margin:0;padding:0 0 0 10px;height:20px;width:140px;border-radius:20px;font-size:10px;' type='text' onkeydown=' ' maxlength=30 value='" + val + "' />"
        + " <a href='#' class='button' onclick='javascript:saveEdit(\"" + id + "\");return false;'>保存</a>"
        + " <a href='#' class='button red' onclick='javascript:cancelEdit();return false;'>取消</a></span>");
}

function saveEdit(id) {
    var cat = $("input", $(e_tr)).val();
    var data = { id: id, name: cat };
    $.get("/Manage/EditLabelName", data);
    e_tr.children("td:eq(0)").children("span:eq(0)").text(cat);
    cancelEdit();
}

function cancelEdit() {
    if (e_tr) {
        e_tr.children("td:eq(0)").children("span:eq(0)").attr("style", "display:inline");
        e_tr.children("td:eq(0)").children("span:eq(1)").remove();
    }
    e_tr = null;
}

function change(id, value) {
    var data = { id: id, level: value };
    $.get("/Manage/EditLabelLevel", data);
}

function addLabel() {
    var name = $("#txtLbl").val().trim();
    if (name.length <= 0) {
        alert("请输入标题名称！");
        $("#txtLbl").focus();
        return;
    }
    var data = { name: name, level: 1 };
    $.get("/Manage/AddLabel", data);
    RefreshTable('#table-example', '/BlogManage/GetLabelData');

}

function RefreshTable(tableId, url) {

    $.getJSON(url, null, function (json) {
        var table = $(tableId).dataTable();
        var oSettings = table.fnSettings();
        table.fnClearTable(this);
        for (var i = 0; i < json.aaData.length; i++) {
            table.oApi._fnAddData(oSettings, json.aaData[i]);
        }
        oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
        table.fnDraw();
    });
}
