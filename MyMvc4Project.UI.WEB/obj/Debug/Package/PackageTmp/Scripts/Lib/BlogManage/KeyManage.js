﻿$(function () {
    $("#alertBox-generated").css("display", "none");
});

function btn_updatekey() {
    var oldpass = $("#oldpassword").val().trim();
    var newpass1 = $("#newpassword1").val().trim();
    var newpass2 = $("#newpassword2").val().trim();
    var count = 0;
    var len = oldpass.length;
    var len1 = newpass1.length;
    var len2 = newpass2.length;
    if (len2 <= 0) {
        $("#newpassword2").focus();
        count++;
    }
    if (len1 <= 0) {
        $("#newpassword1").focus();
        count++;
    }
    if (len <= 0) {
        $("#oldpassword").focus();
        count++;
    }
    $("#oldpassword").toggleClass("error", len <= 0);
    $("#newpassword1").toggleClass("error", len1 <= 0);
    $("#newpassword2").toggleClass("error", len2 <= 0);
    if (count > 0) {
        $("#alertBox-generated").text("你有" + count + "个选项没有输入，请重新输入再点击保存");
        $("#alertBox-generated").css("display", "inherit");
        return;
    }
    if (newpass2 != newpass1) {
        $("#newpassword1").addClass("error");
        $("#newpassword2").addClass("error");
        $("#alertBox-generated").text("两次输入密码不一致，请重新输入再点击保存");
        $("#alertBox-generated").css("display", "inherit");
        return;
    }
    var data = { keyold: oldpass, keynew1: newpass1, keynew2: newpass2 };
    $.getJSON('/Manage/UpdateKey', data, function (json) {
        if (!json.success) {
            alert(json.msg);
        } else {
            location.href = '/Logins/Login';
        }
    });
}

function password_change(e) {
    if ($(e).hasClass('error')) {
        $(e).removeClass("error");
    }
    if ($(e).val().trim().length <= 0) {
        $(e).addClass("error");
    }
}