﻿$(function () {
    blogData();
});

function blogData() {
    $.getJSON('/Manage/TitleData', function (data) {
        if (data == null) {
            return false;
        }
        $("#id").val(data.id);
        $("#bigTitle").val(data.bigTitle);
        $("#smallTitle").val(data.smallTitle);
        $("#template").val(data.blogCss);
    });
}

