﻿$(function () {
    initTable();
    $(".fg-toolbar:first-child").css("display", "none");
});

function initTable() {
    var name = $("#name").text();
    var oTable = $('#table-example').dataTable({
        bLengthChange: false,
        bFilter: false, //搜索栏
        bSort: false, //是否支持排序功能
        bAutoWidth: true,  //自适应宽度
        sPaginationType: "full_numbers",
        bProcessing: false,
        bServerSide: true,
        bDeferRender: true,
        bInfo: false,
        //sScrollY: 300,//垂直固定高度
        sAjaxSource: "/BlogManage/GetArticleData",
        aoColumns: [
        {
            sTitle: "Id",
            mData: 'Id',
            bVisible: false
        },
        {
            sTitle: "标题",
            mData: 'Title',
            sClass: "tTitle",
            fnRender: function (oObj) {
                return '<a href=\"/Blog/Article?name=' + name + '&id=' + oObj.aData.Id + '\">' +
                                oObj.aData.Title + '</a>';
            }
        }, {
            sTitle: "发布状态",
            mData: null,
            sClass: "tClickCount",
            sDefaultContent: '发布'
        },
        {
            sTitle: "页面浏览",
            mData: 'ClickCount',
            sClass: "tClickCount"
        },
        {
            sTitle: "评论",
            mData: 'ClickCount',
            sClass: "tClickCount"
        },
        {
            sTitle: "操作",
            mData: null,
            sClass: "tClickCount",
            fnRender: function (oObj) {
                return '<a href=\"/BlogManage/EditArticle?id=' + oObj.aData.Id + '\">编辑</a> | <a href="javascript:void(0)" onclick="del_click(\'' + oObj.aData.Id + '\')" class="editor_remove">删除</a>';
            }
        }],
        oLanguage: {
            sLengthMenu: "每页显示 _MENU_ 条记录",
            sZeroRecords: "抱歉， 没有找到",
            sInfo: "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
            sInfoEmpty: "没有数据",
            sInfoFiltered: "(从 _MAX_ 条数据中检索)",
            sZeroRecords: "没有检索到数据",
            sSearch: "名称:",
            oPaginate: {
                sFirst: "首页",
                sPrevious: "前一页",
                sNext: "后一页",
                sLast: "尾页"
            }
        }
    });
}


function del_click(id) {
    if (confirm("确定要删除数据？")) {
        zwobj.url = '/Manage/DeleteArticle';
        zwobj.data = { articleid: id };
        ajaxData();
    }
}

function ajax_DeleteArticle() {
    location.href = '/BlogManage/ArticleManage/?action=del';
}


