﻿function login() {
    formobj.url = '/Logins/UserLogin';
    // bind to the form's submit event 
    $('#form-signin').submit(function () {
        $(this).ajaxSubmit(formobj);
        return false;
    });
}

function quit() {
    $('#form-signin')[0].reset();
    location.href = "/Home";
}

function ajax_UserLogin(data) {
    if (data.Other == "admin") {
        location.href = '/Manage/BlogManage';
    } else {
        location.href = '/Blog/Index/' + data.Other;
    }
}

