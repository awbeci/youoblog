﻿$(function () {
    $('#form-register')[0].reset();
});

function register() {
    formobj.url = '/Logins/AddUser';
    formobj.success = function (data) {
        if (data.success == true) {
            location.href = "/Home";
        } else {
            alert("出错！");
        }
    };
    // bind to the form's submit event 
    $('#form-register').submit(function () {
        $(this).ajaxSubmit(formobj);
        return false;
    });
}

function quit() {
    $('#form-register')[0].reset();
    location.href = "/Home";
}

