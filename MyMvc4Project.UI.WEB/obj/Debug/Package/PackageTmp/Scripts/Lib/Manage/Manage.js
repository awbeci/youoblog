﻿Ext.onReady(function () {
    var panel = ExtPanel();
    window.onresize = function () {
        panel.setHeight(document.documentElement.clientHeight);
    };
});

function ExtPanel() {
    var pandel = new Ext.panel.Panel({
        renderTo: 'container',
        width: 1170,
        height: document.documentElement.clientHeight,
        layout: {
            type: 'border',
            padding: '5'
        },
        items: [{
            region: 'north',
            height: 80,
            border: false,
            margin: '0,0,0,0',
            bodyStyle: {
                background: '#3992D4'
            },
            html: '<div class="header"><div class="title">优尔博客后台管理</div><div class="user"><a href="/home">主页</a><a href="/home">个人主页</a><a href="/home">返回</a></div></div>'
        }, {
            region: 'west',
            icon: '../../../Images/extjs/application_side_list.png',
            title: '菜单导航',
            width: 200,
            id: 'MainMenu',
            stateId: 'statePanelExample',
            stateful: true,
            split: true,
            collapsible: true,
            padding: '0',
            layout: 'accordion',
            items: [
                {
                    title: '功能管理',
                    icon: "../../../Images/extjs/text_list_numbers.png",
                    layout: 'fit',
                    items: [{
                        xtype: 'treepanel',
                        border: 0,
                        rootVisible: false,
                        root: {
                            expanded: true,
                            children: [
                                //{ id: '01', text: '新文章', icon: '../../../Images/extjs/page_edit.png', leaf: true, href: '/Manage/NewArticle' },
                                //{ id: '02', text: '文章管理', icon: '../../../Images/extjs/page_white_wrench.png', leaf: true, href: '/Manage/ArticleManage' },
                                //{ id: '03', text: '标签管理', icon: '../../../Images/extjs/page_white_code.png', leaf: true, href: '/Manage/LabelManage' },
                                { id: '04', text: '用户管理', icon: '../../../Images/extjs/group.png', leaf: true, href: '/Manage/UserManage' },
                                { id: '05', text: '通知管理', icon: '../../../Images/extjs/comment.png', leaf: true, href: '/Manage/NoticeManage' }
                            ]
                        }
                    }]
                }
//                , {
//                    title: '博客配置',
//                    icon: "../../../Images/extjs/cog.png",
//                    layout: 'fit',
//                    items: [{
//                        xtype: 'treepanel',
//                        border: 0,
//                        rootVisible: false,
//                        root: {
//                            expanded: true,
//                            children: [
//                                { id: '11', text: '博客设置', icon: '../../../Images/extjs/text_dropcaps.png', leaf: true, href: '/Manage/TitleSet' }
//                                
//                            ]
//                        }
//                    }]
//                }, {
//                    title: '系统菜单',
//                    icon: "../../../Images/extjs/wrench_orange.png",
//                    layout: 'fit',
//                    items: [{
//                        xtype: 'treepanel',
//                        border: 0,
//                        rootVisible: false,
//                        root: {
//                            expanded: true,
//                            children: [
//                            { id: '21', text: '密码修改', icon: '../../../Images/extjs/key.png', leaf: true, href: '/Manage/MyKey' }
//                            ]
//                        }
//                    }]
//                }
            ]
        }, {
            region: 'center',
            xtype: 'tabpanel',
            id: 'MainTabPanel',
            activeTab: 0,
            items: [{
                title: '首页',
                icon: '../../../Images/extjs/house.png',
                html: '<h1>欢迎使用优尔博客1.0系统</h1>'
            }]
        }, {
            region: 'south',
            collapsible: false,
            bodyStyle: {
                background: '#3992D4'
            },
            html: '<div class="footer">© 合肥优尔电子科技有限公司 2014</div>',
            split: false,
            height: 22
        }]
    });
    bindNavToTab("MainMenu", "MainTabPanel");
    return pandel;
}
//-------------------------------------------------------------------------------------

//绑定左边面板上的节点
function bindNavToTab(accordionId, tabId) {
    var accordionPanel = Ext.getCmp(accordionId);
    if (!accordionPanel) return;

    var treeItems = accordionPanel.queryBy(function (cmp) {
        if (cmp && cmp.getXType() === 'treepanel') return true;
        return false;
    });
    if (!treeItems || treeItems.length == 0) return;

    for (var i = 0; i < treeItems.length; i++) {
        var tree = treeItems[i];

        tree.on('itemclick', function (view, record, htmlElement, index, event, opts) {
            if (record.isLeaf()) {
                // 阻止事件传播
                event.stopEvent();

                var href = record.data.href;

                if (!href) return;
                // 修改地址栏
                //window.location.hash = href;
                // 新增Tab节点
                createNewTab(tabId, record.data.id, record.data.text, record.data.icon, href);
            }
        });
    }
}

//在tabpanel里添加新tab节点
function createNewTab(tabpanelId, tabId, tabTitle, tabIcon, iframeSrc) {
    var tabpanel = Ext.getCmp(tabpanelId);
    if (!tabpanel) return;  //未找到tabpanel，返回

    //寻找id相同的tab
    var tab = Ext.getCmp(tabId);
    if (tab) { tabpanel.setActiveTab(tab); return; }

    //新建一个tab，并将其添加到tabpanel中
    //tab = Ext.create('Ext.tab.Tab', );
    tab = tabpanel.add({
        id: tabId,
        icon: tabIcon,
        title: tabTitle,
        closable: true,
        html: '<iframe style="overflow:auto;width:100%; height:100%;" src="' + iframeSrc + '" frameborder="0"></iframe>'
    });
    tabpanel.setActiveTab(tab);
}

function login() {
    location.href = "/Logins/Login";
}