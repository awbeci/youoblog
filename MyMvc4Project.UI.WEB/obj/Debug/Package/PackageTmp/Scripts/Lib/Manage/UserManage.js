﻿var itemsPerPage = 10;   // set the number of items you want per page

//extjs初始化
Ext.onReady(function () {
    gridPanel();

    //调整gridpanel根据屏幕分辨率来显示
    var panel = Ext.getCmp('gridPanel');
    window.onresize = function () {
        panel.setHeight(document.documentElement.clientHeight);
    };
});

//----------------------------------------------------------------------------------

function gridPanel() {
    var panel = new Ext.container.Viewport({
        items: {
            xtype: 'gridpanel',
            id: 'gridPanel',
            store: store,
            height: document.documentElement.clientHeight,
            columns: [
            { header: "Id", hidden: true, flex: 1, sortable: false, dataIndex: 'Id' },
            { header: "用户名", width: 100, align: 'center', sortable: false, dataIndex: 'Name' },
            { header: "密码", width: 80, align: 'center', sortable: false, dataIndex: 'Password' },
            { header: "职称", width: 150, align: 'center', sortable: false, dataIndex: 'PostTitle' },
            { header: "所属部门", width: 150, align: 'center', sortable: false, dataIndex: 'Department' },
            { header: "头像", width: 150, align: 'left', sortable: false, dataIndex: 'Avatar',
                renderer: function (value) {
                    return "<img height='50' width='50' src='" + value + "' />";
                }
            }
            ],
            loadMask: true,
            viewConfig: {
                stripeRows: true
            },
            tbar: ['->',
                { id: 'btn_add', text: '添加', icon: '../../../Images/extjs/add.png', handler: btnAdd },
                { id: 'btn_edit', text: '编辑', icon: '../../../Images/extjs/pencil.png', handler: btnEdit },
                { id: 'btn_delete', text: '删除', icon: '../../../Images/extjs/delete.png', handler: btnDelete }
            ],
            dockedItems: [{
                xtype: 'pagingtoolbar',
                store: store,
                dock: 'bottom',
                displayMsg: '显示 {0} - {1} ，共 {2} 记录',
                displayInfo: true
            }]
        }
    });
}

//grid数据源
var store = Ext.create('Ext.data.Store', {
    pageSize: itemsPerPage,
    fields: ['Id', 'Name', 'Password', 'PostTitle','Department', 'Avatar'],
    remoteSort: true,
    proxy: {
        type: 'ajax',
        url: '/Manage/UserData',
        reader: {
            type: 'json',
            root: 'topics',
            totalProperty: 'totalCount'
        }
    }
});
//grid数据源上传
store.reload({ params: { start: 0, limit: itemsPerPage} });
//-------------------------------------------------------------------------------------------------
/**
* 上传图片验证
**/
function uploadImgCheck(fileObj, fileName) {
    //图片类型验证
    if (!(getImgTypeCheck(getImgHZ(fileName)))) {
        Ext.MessageBox.show({
            title: '提示',
            msg: '上传图片类型有误',
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.INFO
        });
        fileObj.reset(); //清空上传内容
        return;
    }
}

/**
* 图片类型验证
* */
function getImgTypeCheck(hz) {
    var typestr = 'jpg,jpeg,png,gif';
    var types = typestr.split(','); //图片类型
    for (var i = 0; i < types.length; i++) {
        if (hz == types[i]) {
            return true;
        }
    }
    return false;
}

/**
* 获取图片后缀(小写)
* */
function getImgHZ(imgName) {
    //后缀
    var hz = '';
    //图片名称中最后一个.的位置
    var index = imgName.lastIndexOf('.');
    if (index != -1) {
        //后缀转成小写
        hz = imgName.substr(index + 1).toLowerCase();
    }
    return hz;
}

function bp() {
    var curWwwPath = window.document.location.href;
    var pathName = window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0, pos);
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    return (localhostPath + projectName);
}
//-------------------------------------------------------------------------------------------------
function btnAdd() {
    //点击编辑弹出的formpanel
    var formPanel = new Ext.FormPanel({
        bodyStyle: 'padding:5px 20px 0 5px',
        autoScroll: true,
        layout: 'form',
        items: [{
            fieldLabel: '姓名',
            xtype: 'textfield',
            name: 'name',
            allowBlank: false
        }, {
            fieldLabel: '密码',
            xtype: 'textfield',
            name: 'password',
            inputType: 'password',    //密码框属性设置
            allowBlank: false
        }, {
            fieldLabel: '职称',
            xtype: 'textfield',
            name: 'posttitle',
            allowBlank: false
        }, {
            fieldLabel: '所属部门',
            xtype: 'textfield',
            name: 'department',
            allowBlank: false
        }, {
            xtype: 'fileuploadfield',
            id: 'upload',
            fieldLabel: '选择头像',
            buttonText: '请选择...',
            name: 'upload',
            emptyText: '请选择图片',
            blankText: '图片不能为空', listeners: {
                change: function (view, value, eOpts) {
                    uploadImgCheck(view, value);
                }
            }
        }

          ],
        buttonAlign: 'center',
        buttons: [
        {
            text: '保存',
            icon: '../../../Images/extjs/disk.png',
            handler: function () {
                if (!formPanel.getForm().isValid()) {
                    return;
                }
                formPanel.getForm().submit({
                    url: bp() + '/AddUser',
                    method: 'POST',
                    submitEmptyText: false,
                    waitMsg: '正在上传图片,请稍候...',
                    timeout: 60000, // 60s
                    success: function (response, options) {
                        var result = options.result;
                        if (!result.success) {
                            Ext.MessageBox.alert('温馨提示', result.msg);
                            return;
                        } else {
                            win.close(this);
                            store.reload();
                        }
                    },
                    failure: function (response, options) {
                        Ext.MessageBox.alert('温馨提示', options.result.msg);
                    }
                });
            }
        }, {
            text: '关闭',
            icon: '../../../Images/extjs/cross.png',
            handler: function () {
                win.close(this);
            }
        }
      ]
    });

    //点击编辑弹出的windows(formpanel作为window的items)
    var win = Ext.create("Ext.window.Window", {
        title: "添加",       //标题
        draggable: false,
        icon: '../../../Images/extjs/add.png',
        height: 230,                          //高度
        width: 350,                           //宽度
        layout: "fit",                        //窗口布局类型
        modal: true, //是否模态窗口，默认为false
        resizable: false,
        items: [formPanel]
    });
    win.show();
}

function btnEdit() {
    //获取grid选中的数据
    var sm = Ext.getCmp("gridPanel").getSelectionModel();
    var rows = sm.getSelection();
    if (!sm.hasSelection()) {
        Ext.MessageBox.show({
            title: '提示',
            msg: '请选择一行数据进行操作！',
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.INFO
        });
        return;
    }
    var id = rows[0].get('Id');

    //点击编辑弹出的formpanel
    var formPanel = new Ext.FormPanel({
        bodyStyle: 'padding:5px 20px 0 5px',
        autoScroll: true,
        layout: 'form',
        items: [{
            fieldLabel: '职称',
            xtype: 'textfield',
            name: 'posttitle',
            value: rows[0].get('PostTitle'),
            allowBlank: false
        },{
            fieldLabel: '所属部门',
            xtype: 'textfield',
            name: 'department',
            value: rows[0].get('Department'),
            allowBlank: false
        }, {
            xtype: 'fileuploadfield',
            id: 'upload',
            fieldLabel: '选择头像',
            buttonText: '请选择...',
            name: 'upload',
            emptyText: '请选择图片',
            blankText: '图片不能为空',
            listeners: {
                change: function (view, value, eOpts) {
                    uploadImgCheck(view, value);
                }
            }
        }

          ],
        buttonAlign: 'center',
        buttons: [
        {
            text: '保存',
            icon: '../../../Images/extjs/disk.png',
            handler: function () {
                if (!formPanel.getForm().isValid()) {
                    return;
                }
                formPanel.getForm().submit({
                    url: bp() + '/EditUser',
                    params: {
                        id: id
                    },
                    method: 'POST',
                    submitEmptyText: false,
                    waitMsg: '正在上传图片,请稍候...',
                    timeout: 60000, // 60s
                    success: function (response, options) {
                        var result = options.result;
                        if (!result.success) {
                            Ext.MessageBox.alert('温馨提示', result.msg);
                            return;
                        } else {
                            win.close(this);
                            store.reload();
                        }
                    },
                    failure: function (response, options) {
                        Ext.MessageBox.alert('温馨提示', options.result.msg);
                    }
                });
            }
        }, {
            text: '关闭',
            icon: '../../../Images/extjs/cross.png',
            handler: function () {
                win.close(this);
            }
        }
      ]
    });

    //点击编辑弹出的windows(formpanel作为window的items)
    var win = Ext.create("Ext.window.Window", {
        title: "编辑",       //标题
        draggable: false,
        icon: '../../../Images/extjs/pencil.png',
        height: 200,                          //高度
        width: 350,                           //宽度
        layout: "fit",                        //窗口布局类型
        modal: true, //是否模态窗口，默认为false
        resizable: false,
        items: [formPanel]
    });
    win.show();
}

function btnDelete() {
    //获取grid选中的数据
    var sm = Ext.getCmp("gridPanel").getSelectionModel();
    var rows = sm.getSelection();
    if (!sm.hasSelection()) {
        Ext.MessageBox.show({
            title: '提示',
            msg: '请选择一行数据进行操作！',
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.INFO
        });
        return;
    }
    Ext.Msg.show({
        title: '删除',
        buttons: Ext.MessageBox.YESNO,
        msg: '确定删除这条数据?',
        icon: Ext.MessageBox.QUESTION,
        fn: function (btn) {
            if (btn == 'yes') {
                Ext.Ajax.request({
                    url: '/Manage/DeleteUser',
                    params: {
                        id: rows[0].get('Id')
                    },
                    method: 'POST',
                    success: function (response) {
                        var obj = Ext.JSON.decode(response.responseText);
                        if (obj.success) {
                            store.reload();
                        }
                    },
                    failure: function (form, action) {
                        Ext.Msg.alert('Error', 'ajax提交失败，请联系管理员！');
                    }
                });
            }
        }
    });
}