﻿var itemsPerPage = 10;   // set the number of items you want per page

//extjs初始化
Ext.onReady(function () {
    gridPanel();

    //调整gridpanel根据屏幕分辨率来显示
    var panel = Ext.getCmp('gridPanel');
    window.onresize = function () {
        panel.setHeight(document.documentElement.clientHeight);
    };
});

//----------------------------------------------------------------------------------

function gridPanel() {
    var panel = new Ext.container.Viewport({
        items: {
            xtype: 'gridpanel',
            id: 'gridPanel',
            store: store,
            height: document.documentElement.clientHeight,
            columns: [
            { header: "Id", hidden: true, flex: 1, sortable: false, dataIndex: 'Id' },
            { header: "标题", width: 250, sortable: false, dataIndex: 'Title' },
            { header: "内容", width: 470, sortable: false, dataIndex: 'Contents',
                renderer: function (value) {
                    return decodeURI(value).replace(/<[^>].*?>/g, "");
                }
            },
            { header: "点击数", width: 80, align: 'center', sortable: false, dataIndex: 'ClickCount' },
            { header: "发表时间", width: 150, align: 'center', sortable: false, dataIndex: 'CreateTime',
                renderer: function (value) {
                    return eval("new " + value.split('/')[1]).Format("yyyy-MM-dd HH:mm:ss");
                }
            }
            ],
            loadMask: true,
            viewConfig: {
                stripeRows: true
            },
            tbar: ['->',
                { id: 'btn_edit', text: '编辑', icon: '../../../Images/extjs/pencil.png', handler: btnEdit },
                { id: 'btn_delete', text: '删除', icon: '../../../Images/extjs/delete.png', handler: btnDelete }
            ],
            dockedItems: [{
                xtype: 'pagingtoolbar',
                store: store,
                dock: 'bottom',
                displayMsg: '显示 {0} - {1} ，共 {2} 记录',
                displayInfo: true
            }]
        }
    });
}

//grid数据源
var store = Ext.create('Ext.data.Store', {
    pageSize: itemsPerPage,
    fields: ['Id', 'Title', 'Contents', 'ClickCount', 'CreateTime'],
    remoteSort: true,
    proxy: {
        type: 'ajax',
        url: '/Manage/ArticleData',
        reader: {
            type: 'json',
            root: 'topics',
            totalProperty: 'totalCount'
        }
    }
});
//grid数据源上传
store.reload({ params: { start: 0, limit: itemsPerPage} });

//-----------------------------------------------------------------------

//点击编辑按钮编辑grid数据
function btnEdit() {
    //获取grid选中的数据
    var sm = Ext.getCmp("gridPanel").getSelectionModel();
    var rows = sm.getSelection();
    if (!sm.hasSelection()) {
        Ext.MessageBox.show({
            title: '提示',
            msg: '请选择一行数据进行操作！',
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.INFO
        });
        return;
    }

    var myCheckboxGroup; //定义checkboxgroup组件

    //通过 Ext.Ajax.request()方法获取用户文章关联的标签数据
    Ext.Ajax.request({
        url: '/Manage/GetLableNameByUserIdAndArticleId',
        async: false,
        params: {
            articleid: rows[0].get('Id')
        },
        success: function (response) {
            var obj = Ext.JSON.decode(response.responseText);
            if (!obj.success && obj.errors.msg == "login") {
                parent.login();
                return;
            }
            else if (!obj.success) {
                Ext.MessageBox.show({
                    title: '提示',
                    msg: obj.errors.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            }
            var len1 = obj.data.alldata.length;
            var len2 = obj.data.mydata.length;

            var myCheckboxItems = []; //创建checkbox 数据项
            for (var i = 0; i < len1; i++) {
                var checkboxObj = {
                    boxLabel: obj.data.alldata[i].Name,
                    name: 'cbLable',
                    inputValue: obj.data.alldata[i].Id,
                    padding: '0 20px 5px 0',
                    checked: false
                };
                for (var j = 0; j < len2; j++) {
                    if (obj.data.alldata[i].Id == obj.data.mydata[j].Id) {
                        checkboxObj.checked = true;
                    }
                }
                myCheckboxItems.push(checkboxObj); //保存checkbox 数据项
            }
            //新建checkboxgroup组件(将checkboxItems数据项作为它的数据项)
            myCheckboxGroup = new Ext.form.CheckboxGroup({
                xtype: 'checkboxgroup',
                id: 'cbgLable',
                columns: 6,
                items: myCheckboxItems
            });
        },
        failure: function (response, opts) {
            var obj = Ext.JSON.decode(response.responseText);
            Ext.Msg.alert('Error', obj.msg);
        }
    });
    if (myCheckboxGroup == undefined) {
        return;
    }
    //点击编辑弹出的formpanel
    var formPanel = new Ext.FormPanel({
        //labelAlign: 'top',
        bodyStyle: 'padding:5px 20px 0 5px',
        autoScroll: true,
        layout: 'form',
        items: [{
            xtype: 'textfield',
            name: 'title',
            anchor: '100%'
        }, {
            xtype: 'htmleditor',
            border: true,
            id: 'context',
            name: 'context',
            plugins: [
                    Ext.create('Ext.zc.form.HtmlEditorImage')
                ],
            height: 400,
            anchor: '100%'
        },
            myCheckboxGroup
          ],
        buttonAlign: 'center',
        buttons: [
        {
            text: '保存',
            icon: '../../../Images/extjs/disk.png',
            handler: function () {
                if (!formPanel.getForm().isValid()) {
                    return;
                }
                //----------------获取选中的checkbox值(start)-----------------
                var chkLableValue = Ext.getCmp('cbgLable').getChecked();
                var lblValue = [], lblName = [];
                Ext.Array.each(chkLableValue, function (item) {
                    lblValue.push(item.inputValue);
                    lblName.push(item.boxLabel);
                });
                //----------------获取选中的checkbox值(end)-----------------
                Ext.Ajax.request({
                    url: '/Manage/UpdateArticle',
                    method: 'get',
                    params: {
                        id: rows[0].get('Id'),
                        title: formPanel.getForm().findField("title").getValue(),
                        context: encodeURI(formPanel.getForm().findField("context").getValue()),
                        lblId: lblValue,
                        lblName: lblName
                    },
                    success: function (response) {
                        var obj = Ext.JSON.decode(response.responseText);
                        if (obj.success) {
                            win.close(this);
                            store.reload();
                        }
                    },
                    failure: function (form, action) {
                        Ext.Msg.alert('Error', 'ajax提交失败，请联系管理员！');
                        win.close(this);
                    }
                });
            }
        }, {
            text: '关闭',
            icon: '../../../Images/extjs/cross.png',
            handler: function () {
                win.close(this);
            }
        }
      ]
    });

    //点击编辑弹出的windows(formpanel作为window的items)
    var win = Ext.create("Ext.window.Window", {
        title: "编辑",       //标题
        draggable: false,
        icon: '../../../Images/extjs/pencil.png',
        height: 600,                          //高度
        width: 850,                           //宽度
        layout: "fit",                        //窗口布局类型
        modal: true, //是否模态窗口，默认为false
        resizable: false,
        items: [formPanel]
    });

    //给form重置后赋值
    formPanel.getForm().reset();
    var contents = rows[0].get('Contents');
    formPanel.getForm().findField("title").setValue(rows[0].get('Title'));
    formPanel.getForm().findField("context").setValue(decodeURI(contents));
    //window显示
    win.show();
}

//点击删除按钮删除grid数据
function btnDelete() {
    //获取grid选中的数据
    var sm = Ext.getCmp("gridPanel").getSelectionModel();
    var rows = sm.getSelection();
    if (!sm.hasSelection()) {
        Ext.MessageBox.show({
            title: '提示',
            msg: '请选择一行数据进行操作！',
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.INFO
        });
        return;
    }
    Ext.Msg.show({
        title: '删除',
        buttons: Ext.MessageBox.YESNO,
        msg: '确定删除这条数据?',
        icon: Ext.MessageBox.QUESTION,
        fn: function (btn) {
            if (btn == 'yes') {
                Ext.Ajax.request({
                    url: '/Manage/DeleteArticle',
                    params: {
                        articleid: rows[0].get('Id')
                    },
                    method: 'POST',
                    success: function (response) {
                        var obj = Ext.JSON.decode(response.responseText);
                        if (obj.success) {
                            store.reload();
                        }
                    },
                    failure: function (form, action) {
                        Ext.Msg.alert('Error', 'ajax提交失败，请联系管理员！');
                    }
                });  
            }
        }
    });
}