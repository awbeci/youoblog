﻿$(function () {
    scrollUp();
    getAllLable();
    getArchives();
    SyntaxHighlighter.all();
});


//------------------------------------------------------------------------------------------
function scrollUp() {
    $.scrollUp({
        scrollName: 'scrollUp', // Element ID
        topDistance: '300', // Distance from top before showing element (px)
        topSpeed: 300, // Speed back to top (ms)
        animation: 'fade', // Fade, slide, none
        animationInSpeed: 200, // Animation in speed (ms)
        animationOutSpeed: 200, // Animation out speed (ms)
        scrollText: '', // Text for element
        activeOverlay: false  // Set CSS color to display scrollUp active point, e.g '#00FFFF'
    });
}

function getAllLable() {
    zwobj.url = Resolve2('/Blog/MyGetAllByUserId');
    ajaxData();
}

function getArchives() {
    zwobj.url = Resolve2('/Blog/GetArchives');
    ajaxData();
}

function newArticle() {
    location.href = "/Manage/BlogManage";
}
//-----------------------------------------------------------------------------------------------

function userLogin() {
    zwobj.url = Resolve2('/Blog/LoginUserMainPage');
    ajaxData();
}

function btn_submit(id) {
    formobj.url = '/Blog/SubmitComment/?' + id;
    $('#form-comment').submit(function () {
        $(this).ajaxSubmit(formobj);
        return false;
    });
}

function btn_login() {
    location.href = Resolve2("/Logins/Login");
}

//------------------------------------------------------------------------------------------------

function ajax_SubmitComment(data) {
    location.reload();
}

function ajax_GetUserLable(data) {
    $(".list-tag").empty();
    var html = "";
    var len = data.Data.length;
    for (var i = 0; i < len; i++) {
        html += '<li class="level-' + data.Data[i].Level +
            '"><a href="/Blog/ArticleList?name=' + data.Other + '&para=' + data.Data[i].Name + '&type=lb">'
            + data.Data[i].Name + '</a></li>';
    }
    $(".list-tag").append(html);
}

function ajax_GetArchives(data) {
    $(".list-archives").empty();
    var html = "";
    var len = data.Data.length;
    for (var i = 0; i < len; i++) {
        html += '<li><a href="/Blog/ArticleList?name=' + data.Other + '&para=' + data.Data[i][2] + '&type=dt">'
            + data.Data[i][0] + '(' + data.Data[i][1] + ')' + '</a></li>';
    }
    $(".list-archives").append(html);
}

function ajax_LoginUserMainPage(data) {
    location.href = data.Other;
}