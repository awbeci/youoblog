﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>博客管理</title>
    <link href="../../Scripts/extjs/resources/css/ext-all-neptune.css" rel="stylesheet"
        type="text/css" />
    <script src="../../Scripts/extjs/ext-all.js" type="text/javascript"></script>
    <script src="../../Scripts/extjs/ext-theme-neptune.js" type="text/javascript"></script>
    <script src="../../Scripts/extjs/locale/ext-lang-zh_CN.js" type="text/javascript"></script>
    <script src="../../Scripts/zwjs.js" type="text/javascript"></script>
    <script src="../../Scripts/Lib/Manage/HtmlEditorImage.js" type="text/javascript"></script>
    <script src="../../Scripts/Lib/Manage/ArticleManage.js" type="text/javascript"></script>
    <style>
        .heditImgIcon
        {
            background-image: url(../../Images/extjs/image_add.png) !important;
        }
    </style>
</head>
<body>
</body>
</html>
