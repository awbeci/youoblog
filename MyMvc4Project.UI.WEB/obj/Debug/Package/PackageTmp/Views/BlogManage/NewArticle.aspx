﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/BlogManage/BlogManage.Master"
    Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<%@ Import Namespace="MyMvc4Project.Dal.Views" %>
<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>新文章 </title>
    <link href="../../Scripts/kindeditor-4.1.10/themes/default/default.css" rel="stylesheet"
        type="text/css" />
    <link href="../../Scripts/kindeditor-4.1.10/plugins/code/prettify.css" rel="stylesheet"
        type="text/css" />
    <script src="../../Scripts/jquery/jquery.form.js" type="text/javascript"></script>
    <script src="../../Scripts/kindeditor-4.1.10/kindeditor-all-min.js" type="text/javascript"></script>
    <script src="../../Scripts/kindeditor-4.1.10/lang/zh_CN.js" type="text/javascript"></script>
    <script src="../../Scripts/kindeditor-4.1.10/plugins/code/code.js" type="text/javascript"></script>
    <script src="../../Scripts/kindeditor-4.1.10/plugins/code/prettify.js" type="text/javascript"></script>
    <script src="../../Scripts/zwjs.js" type="text/javascript"></script>
    <script src="../../Scripts/Lib/Manage/NewArticle.js" type="text/javascript"></script>
    <style>
       
        .kedit
        {
            margin-top: 15px;
            background: url("../../images/shine-effect.png") repeat-x scroll 0 0 rgba(33,40,44,0.7);
            border: 1px solid #25333c;
            border-radius: 5px 5px 5px 5px;
            padding: 8px;
            box-shadow: 0 0 5px rgba(0,0,0,0.5);
            width: 1000px;
            margin-left: 40px;
        }
       
        #content-js
        {
            width: 99%;
            height: 400px;
        }
        #txt_title
        {
            width: 99%;
            margin-right: 10px;
        }
        .block-header {
           margin: 0;
       }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="breadcrumb">
        <ul id="breadcrumbs">
            <li><a href="/BlogManage/index" title="主页"><span id="bc-home"></span></a></li>
            <li>新文章</li>
        </ul>
        <div class="shadow-bottom">
        </div>
    </div>
    <div id="main-content">
        <div class="header">
            <h1 class="page-title">
                新文章</h1>
            <p style="font-size: 10px;">
                在这里添加您的新文章</p>
        </div>
        <div class="kedit">
            <div class="block-header">
                <h1>
                    添加新文章
                </h1>
                <span></span>
            </div>
            <form id="form-article" name="form-article" method="POST">
            <p>
                标题：</p>
            <input type="text" id="txt_title" name="title" />
            <p>
                正文：</p>
            <textarea id="content-js" name="content"></textarea>
            <p>
                <% var lable = ViewData["lable"] as IList<LableView>;
                   if (lable != null && lable.Count != 0)
                   {
                %>
                <p>
                    标签：</p>
                <%
                       foreach (var view in lable)
                       {%>
                <input class="chk-lable" name="lable" type="checkbox" value="<%= view.Name %>"><%= view.Name %></input>
                <%} %>
                <% }
                   else
                   { %>
                <p>
                    无标签</p>
                <% } %>
            </p>
            </form>
            <div class="block-actions">
                <ul class="actions-left">
                    <li><a class="button red" id="reset-validate-form" href="javascript:void(0);" >取消 </a>
                    </li>
                </ul>
                <ul class="actions-right">
                    <li>
                        <input type="submit" class="button" value="发布" onclick="btn_submit()" />>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</asp:Content>
