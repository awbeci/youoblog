﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/BlogManage/BlogManage.Master"
    Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link href="../../Scripts/datatable-1.9.4/media/css/demo_page.css" rel="stylesheet"
        type="text/css" />
    <script src="../../Scripts/datatable-1.9.4/media/js/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../Scripts/zwjs.js" type="text/javascript"></script>
    <script src="../../Scripts/Lib/BlogManage/LabelManage.js" type="text/javascript"></script>
    <style>
        .block-border
        {
            width: 30%;
        }
        .lTitle
        {
            width: 55%;
        }
        .lClass
        {
            width: 15%;
        }
        .label-input
        {
            padding: 5px 10px;
            margin: 0 1px;
            background-color: #fafafa;
            height: 30px;
        }
        .block-content
        {
            border-bottom: none;
        }
        #txtLbl
        {
            height: 23px;
            margin: 0;
            padding: 0;
            border-radius: 20px;
            padding-left: 10px;
        }
        .levelSel
        {
            margin: 0;
            padding: 0;
            width: 40px;
            height: 20px;
            font-size: 12px;
        }
        .table th, .table td
        {
            padding: 8px 4px;
        }
        #table-example tbody tr td:first-child
        {
            padding: 4px;
            padding-left: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="breadcrumb">
        <ul id="breadcrumbs">
            <li><a href="/BlogManage/index" title="主页"><span id="bc-home"></span></a></li>
            <li>标签管理</li>
        </ul>
        <div class="shadow-bottom">
        </div>
    </div>
    <div id="main-content">
        <div class="header">
            <h1 class="page-title">
                标签管理</h1>
            <p style="font-size: 10px;">
                在这里管理您的标签</p>
        </div>
        <div class="block-border">
            <div class="block-header">
                <h1>
                    管理标签
                </h1>
                <span></span>
            </div>
            <div class="block-content">
                <div class="dataTables_wrapper" id="table-example_wrapper">
                    <table id="table-example" class="table">
                    </table>
                </div>
            </div>
            <div class="label-input">
                <input type="text" id="txtLbl" name="title">
                <input type="submit" onclick="addLabel();" class="button" value="添加标题" />
            </div>
        </div>
    </div>
</asp:Content>
