﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/BlogManage/BlogManage.Master"
    Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<%@ Import Namespace="MyMvc4Project.Models" %>
<%@ Import Namespace="NHibernate.Collection.Generic" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>新文章 </title>
    <link href="../../Scripts/kindeditor-4.1.10/themes/default/default.css" rel="stylesheet"
        type="text/css" />
    <link href="../../Scripts/kindeditor-4.1.10/plugins/code/prettify.css" rel="stylesheet"
        type="text/css" />
    <script src="../../Scripts/jquery/jquery.form.js" type="text/javascript"></script>
    <script src="../../Scripts/kindeditor-4.1.10/kindeditor-all-min.js" type="text/javascript"></script>
    <script src="../../Scripts/kindeditor-4.1.10/lang/zh_CN.js" type="text/javascript"></script>
    <script src="../../Scripts/kindeditor-4.1.10/plugins/code/code.js" type="text/javascript"></script>
    <script src="../../Scripts/kindeditor-4.1.10/plugins/code/prettify.js" type="text/javascript"></script>
    <script src="../../Scripts/zwjs.js" type="text/javascript"></script>
    <script src="../../Scripts/Lib/BlogManage/EditArticle.js" type="text/javascript"></script>
    <style>
        .kedit
        {
            margin-top: 15px;
            background: url("../../images/shine-effect.png") repeat-x scroll 0 0 rgba(33,40,44,0.7);
            border: 1px solid #25333c;
            border-radius: 5px 5px 5px 5px;
            padding: 8px;
            box-shadow: 0 0 5px rgba(0,0,0,0.5);
            width: 1000px;
            margin-left: 40px;
        }
        
        #content-js
        {
            width: 99%;
            height: 400px;
        }
        #txt_title
        {
            width: 99%;
            margin-right: 10px;
        }
        .block-header
        {
            margin: 0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="breadcrumb">
        <ul id="breadcrumbs">
            <li><a href="/BlogManage/index" title="主页"><span id="bc-home"></span></a></li>
            <li>编辑文章</li>
        </ul>
        <div class="shadow-bottom">
        </div>
    </div>
    <div id="main-content">
        <div class="header">
            <h1 class="page-title">
                编辑文章</h1>
            <p style="font-size: 10px;">
                在这里编辑您的文章</p>
        </div>
        <div class="kedit">
            <div class="block-header">
                <h1>
                    编辑文章
                </h1>
                <span></span>
            </div>
            <form id="form-article" name="form-article" method="POST">
            <p>
                标题：</p>
            <% var data = ViewData["article"] as IEnumerable<Article>;
               string id = "";
               string title = "";
               string content = "";
               IList<Lable> lblList = new List<Lable>();
               if (data != null)
               {
                   foreach (var article in data)
                   {
                       title = article.Title;
                       content = article.Contents;
                       lblList = article.Lables;
                       id = article.Id;
                   }
               }
            %>
            <input type="text" id="txt_title" name="title" value="<%= title %>" />
            <p>
                正文：</p>
            <textarea id="content-js" name="content"><%= HttpContext.Current.Server.UrlDecode(content)%></textarea>
            <p>
                <% var lable = ViewData["label"] as IEnumerable<Lable>;
                   if (lable != null)
                   {
                %>
                <p>
                    标签：</p>
                <% foreach (var view in lable)
                   {
                       if (lblList.Contains(view))
                       {%>
                <input class="chk-lable" name="lable" type="checkbox" checked="checked" value="<%= view.Id %>"><%= view.Name %></input>
                <%}
                       else
                       {%>
                <input class="chk-lable" name="lable" type="checkbox" value="<%= view.Id %>"><%= view.Name %></input>
                <%}%>
                <%}%>
                <% }
                   else
                   { %>
                <p>
                    无标签</p>
                <% } %>
            </p>
            </form>
            <div class="block-actions">
                <ul class="actions-left">
                    <li><a class="button red" id="reset-validate-form" href="javascript:void(0);">取消 </a>
                    </li>
                </ul>
                <ul class="actions-right">
                    <li>
                        <input type="submit" class="button" value="发布" onclick="btn_submit()" />> </li>
                </ul>
            </div>
        </div>
    </div>
    <input type="hidden" id="hiddid" value="<%= id %>" />
</asp:Content>
