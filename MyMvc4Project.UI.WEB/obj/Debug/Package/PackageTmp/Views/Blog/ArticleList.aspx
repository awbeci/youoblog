﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<%@ Import Namespace="MyMvc4Project.Dal.Views" %>
<%@ Import Namespace="MyMvc4Project.Models" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>优尔博客</title>
    <!--Bootstrap css-->
    <link rel="shortcut icon" href="../../Scripts/bootstrap-3.1.0/dist/ico/favicon.ico" />
    <link href="../../Scripts/bootstrap-3.1.0/dist/css/bootstrap.css" rel="stylesheet"
        type="text/css" />
    <!--jquery page css-->
    <link href="../../Scripts/bootstrap-paginator-master/bootstrap-combined.min.css"
        rel="stylesheet" type="text/css" />
    <link href="../../Scripts/bootstrap-3.1.0/jumbotron/jumbotron.css" rel="stylesheet"
        type="text/css" />
    <!--本页面 css-->
     <% var userdata = ViewData["Data"] as User;
       if (userdata != null)
       {%>
    <link href="<%= userdata.Blog.BlogCss %>" rel="stylesheet" type="text/css" />
    <% }
       else
       {%>
    <link href="../../Scripts/Lib/Blog/Blog.css" rel="stylesheet" type="text/css" />
    <%}%>
    <link href="../../Scripts/Lib/Blog/ArticleList.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="../../Scripts/jparse-0.3.3/jparse-beta-0.3.3-full.js" type="text/javascript"></script>
    <!--jquery-scrollUp js-->
    <script src="../../Scripts/jquery.scrollUp.min.js" type="text/javascript"></script>
    <!--bootstrap js-->
    <script src="../../Scripts/bootstrap-3.1.0/dist/js/bootstrap.js" type="text/javascript"></script>
    <!--jquery page js-->
    <script src="../../Scripts/bootstrap-paginator-master/bootstrap-paginator.js" type="text/javascript"></script>
    <script src="../../Scripts/HtmlHelper.js" type="text/javascript"></script>
    <script src="../../Scripts/zwjs.js" type="text/javascript"></script>
    <!--本页面 js-->
    <script src="../../Scripts/Lib/Blog/ArticleList.js" type="text/javascript"></script>
</head>
<body>
    <div class="container">
        <div class="blog-header">
           <% if (userdata != null)
               {%>
            <h1 class="blog-title">
                <%= userdata.Blog.BigTitle%></h1>
            <p class="lead blog-description">
                <%= userdata.Blog.SmallTitle%></p>
            <% }
               else
               {%>
            <h1 class="blog-title">
            </h1>
            <p class="lead blog-description">
            </p>
            <%}%>
        </div>
        <div class="row">
            <div class="col-sm-9 blog-main">
                <!-- /.blog-post -->
                <% foreach (var view in Model)
                   {%>
                <div class="blog-post">
                    <h3 class="blog-post-title">
                        <a href="/Blog/Article?name=<%= Html.Encode(view.User.Name)%>&id=<%= Html.Encode(view.Id)%>">
                            <%= Html.Encode(view.Title)%>
                        </a>
                    </h3>
                    <p class="blog-post-meta">
                        <a href="/Blog/Article?name=<%= Html.Encode(view.User.Name)%>&id=<%= Html.Encode(view.Id)%>">
                            <%= view.CreateTime.ToString("yyyy年MM月dd日 H时m分")%>
                            |
                            <%= Html.Encode(view.User.Name)%>
                        </a>
                    </p>
                    <p>
                        <% string contents = view.Contents;
                           if (contents.Length > 400)
                           {
                               contents = contents.Substring(0, 400) + "...";
                           }
                           var data = HttpContext.Current.Server.UrlDecode(contents);
                           data = Regex.Replace(data, "<[^>].*?>", "");
                           Response.Write(data);
                        %>
                    </p>
                    <p>
                        &nbsp;</p>
                    <div class="tag">
                        Tags：
                        <% foreach (var views in view.Lables)
                           {
                        %>
                        <a href="/Blog/ArticleList?name=<%= Html.Encode(view.User.Name)%>&para=<%= Html.Encode(views.Name)%>&type=lb">
                            <%=views.Name %>
                        </a>&nbsp;&nbsp;
                        <%} %>
                    </div>
                </div>
                <%} %>
                <footer><p>© 合肥优尔电子科技有限公司 2014</p></footer>
           </div>
            <!-- /.blog-main -->
            <div class="col-sm-3 col-sm-offset-0 blog-sidebar">
                <div class="sidebar-module">
                    <h4>
                        导航</h4>
                    <ol class="list-unstyled">
                        <li><a href="/home">首页</a></li>
                        <li><a onclick="userLogin()" href="#">个人主页</a></li>
                        <li><a href="/BlogManage/NewArticle">新文章</a></li>
                        <li><a href="/BlogManage/ArticleManage">管理</a></li>
                        <li><a href="#">关于</a></li>
                    </ol>
                </div>
                <div class="sidebar-module">
                    <h4>
                        标签</h4>
                    <ol class="list-unstyled list-tag">
                    </ol>
                </div>
                <div class="sidebar-module">
                    <h4>
                        档案</h4>
                    <ol class="list-unstyled list-archives">
                    </ol>
                </div>
                <div class="sidebar-module">
                    <h4>
                        联系我</h4>
                    <ol class="list-unstyled">
                        <li><a href="#">GitHub</a></li>
                        <li><a href="#">Twitter</a></li>
                        <li><a href="#">Facebook</a></li>
                    </ol>
                </div>
            </div>
            <!-- /.blog-sidebar -->
        </div>
        <!-- /.row -->
    <!-- /.container -->
    <%-- <div class="blog-footer">
      <p>博客模板制作 <a href="http://getbootstrap.com">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.</p>
      <p>
        <a href="#">回到顶部</a>
      </p>
    </div>--%>
    
        </div>
    <!-- /container -->
    <a id="scrollUp" href="#top" title="" style="position: fixed; z-index: 2147483647;
        display: block;"></a>
</body>
</html>
