﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>优尔博客</title>
    <!--Bootstrap css-->
    <link rel="shortcut icon" href="../../Scripts/bootstrap-3.1.0/dist/ico/favicon.ico" />
    <link href="../../Scripts/bootstrap-3.1.0/dist/css/bootstrap.css" rel="stylesheet"
        type="text/css" />
    <!--jquery page css-->
    <link href="../../Scripts/bootstrap-paginator-master/bootstrap-combined.min.css"
        rel="stylesheet" type="text/css" />
    <link href="../../Scripts/bootstrap-3.1.0/jumbotron/jumbotron.css" rel="stylesheet"
        type="text/css" />
    <!--本页面 css-->
    <link href="../../Scripts/Lib/Home/Home.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="../../Scripts/jparse-0.3.3/jparse-beta-0.3.3-full.js" type="text/javascript"></script>
    <!--jquery-scrollUp js-->
    <script src="../../Scripts/jquery.scrollUp.min.js" type="text/javascript"></script>
    <!--bootstrap js-->
    <script src="../../Scripts/bootstrap-3.1.0/dist/js/bootstrap.js" type="text/javascript"></script>
    <!--jquery page js-->
    <script src="../../Scripts/bootstrap-paginator-master/bootstrap-paginator.js" type="text/javascript"></script>
    <script src="../../Scripts/HtmlHelper.js" type="text/javascript"></script>
    <script src="../../Scripts/zwjs.js" type="text/javascript"></script>
    <script src="../../Scripts/Lib/Home/Home.js" type="text/javascript"></script>
    <style>
        .color-lbl 
        {
            margin: 0;
            padding: 0;
            color: #267CB2;
            font-size: 10px;
            font-weight: normal;
        }
    </style>
</head>
<body>
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span><span
                        class="icon-bar"></span><span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">优尔博客</a>
                <div id="colspan" class="glyphicon glyphicon-chevron-down">
                </div>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="/Home">首页</a></li>
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">日常工作
                        <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="http://www.css88.com/jqapi-1.9/" target="_blank">
                                <img width="16" height="16" src="../../Images/favicon/jquery.ico" />JQuery API</a></li>
                            <li><a href="http://www.sencha.com/products/extjs/" target="_blank">
                                <img width="16" height="16" src="../../Images/favicon/extjs.ico" />ExtJs</a></li>
                            <li><a href="http://www.jeasyui.com/" target="_blank">
                                <img width="16" height="16" src="../../Images/favicon/easyui.ico" />EasyUI</a></li>
                            <li><a href="http://www.ztree.me/v3/main.php" target="_blank">
                                <img width="16" height="16" src="../../Images/favicon/ztree.ico" />ZTree</a></li>
                            <li><a href="http://www.highcharts.com/" target="_blank">
                                <img width="16" height="16" src="../../Images/favicon/highcharts.ico" />HighCharts</a></li>
                            <li><a href="http://www.my97.net/dp/demo/resource/main.asp" target="_blank">
                                <img width="16" height="16" src="../../Images/favicon/calendar.png" />WdatePicker</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">技术站点
                        <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="http://msdn.microsoft.com/zh-cn/dd796167.aspx" target="_blank">
                                <img width="16" height="16" src="../../Images/favicon/msdn.ico" />MSDN </a></li>
                            <li><a href="http://www.asp.net/" target="_blank">
                                <img width="16" height="16" src="../../Images/favicon/aspx.ico" />Asp.Net </a>
                            </li>
                            <li><a href="http://www.codeplex.net/" target="_blank">
                                <img width="16" height="16" src="../../Images/favicon/codeplex.ico" />CodePlex </a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="http://www.cnblogs.com/" target="_blank">
                                <img width="16" height="16" src="../../Images/favicon/cnblogs.ico" />CnBlogs </a>
                            </li>
                            <li><a href="http://www.csdn.net/" target="_blank">
                                <img width="16" height="16" src="../../Images/favicon/csdn.ico" />CSDN </a></li>
                            <li><a href="http://www.51cto.com/" target="_blank">
                                <img width="16" height="16" src="../../Images/favicon/51cto.ico" />51CTO </a>
                            </li>
                            <li><a href="http://www.iteye.com/" target="_blank">
                                <img width="16" height="16" src="../../Images/favicon/iteye.ico" />ITEye </a>
                            </li>
                            <li><a href="http://www.bootcss.com/" target="_blank">
                                <img width="16" height="16" src="../../Images/favicon/bootstrap.ico" />BootStrap
                            </a></li>
                        </ul>
                    </li>
                    <li><a href="Home/About" target="_blank">关于</a></li>
                    <li><a href="http://weibo.com/waishow" target="_blank" title="联系我">
                        <img src="../../Images/sina.jpg" width="20" height="20" /></a></li>
                </ul>
                <div class="navbar-form navbar-right">
                    <% var name = ViewData["name"];
                       if (name == null)
                       {%>
                    <button type="submit" class="btn btn-success" onclick="login()">
                        登 入</button>
                         <button type="submit" class="btn btn-success" onclick="regist()">
                        注 册</button>
                    <%}
                       else
                       {
                           if (name.ToString() == "admin")
                           {%>
                    <%= Html.ActionLink(name.ToString(), "BlogManage", "Manage")%>
                    <button type="submit" class="btn btn-success" onclick="quit()">
                        退 出</button>
                    <%}
                           else
                           {%>
                           <%= Html.ActionLink(name.ToString(), "Index", "Blog", new {id = name.ToString() }, null)%>
                    <button type="submit" class="btn btn-success" onclick="quit()">
                        退 出</button>
                           <%}
                    %>
                    <%}%>
                </div>
            </div>
            <!--/.navbar-collapse -->
        </div>
    </div>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron" id="jumbotron">
        <div class="container" id="container">
            <h1>
                YouoBlog 1.0</h1>
            <p>
                这是一个公司内部交流使用的博客系统，它服务于公司内部人员更好的进行团队协作和免除了face-to-face的交流机会， 它的诞生标志着我们迈向了一个新的阶段。</p>
            <p>
                <a class="btn btn-primary btn-lg" role="button">了解更多 &raquo;</a></p>
        </div>
    </div>
    <div class="container">
        <div id="container-left">
            <div class="alert alert-info">
                <span class="glyphicon glyphicon-info-sign"></span>&nbsp;<strong>温馨提示：</strong>请使用IE9+,Chrome,FireFox,OPera,Safari等支持Html5+CSS3的浏览器，否则会出现浏览失效！</div>
            <div class="container-blog">
                <table width="100%" border="0" cellspacing="0" cellpadding="2" class="content_list"
                    summary="youo blog">
                    <tbody>
                    </tbody>
                </table>
                <div id="example">
                </div>
                <hr />
                <footer>
        <p>&copy; 合肥优尔电子科技有限公司 2014</p>
      </footer>
            </div>
        </div>
        <div id="container-right">
            <div class="input-group">
                <input type="text" class="form-control" />
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="button">
                        搜索</button>
                </span>
            </div>
            <div class="panel panel-note">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="glyphicon myglyphicon">&nbsp;公司通知</span>
                    </h3>
                </div>
                <div class="panel-body">
                </div>
            </div>
            <div class="panel panel-join">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="glyphicon myglyphicon">&nbsp;今日新闻</span>
                    </h3>
                </div>
                <div class="panel-body">
                </div>
            </div>
           <%-- <div class="panel panel-sign">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="glyphicon myglyphicon">&nbsp;热门标签</span></h3>
                </div>
                <div class="panel-body">
                </div>
            </div>--%>
            <div class="panel panel-code">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="glyphicon myglyphicon">&nbsp;公司博客</span></h3>
                </div>
                <div class="panel-body">
                </div>
            </div>
            <div class="panel panel-msdn">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="glyphicon myglyphicon">&nbsp;微软MVP博客</span></h3>
                </div>
                <div class="panel-body">
                    <table border="0" cellpadding="0" cellspacing="3" width="100%">
                        <tbody>
                            <tr>
                                <td align="center">
                                    <a href="http://supper3000.cnblogs.com/" target="_blank">
                                        <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_sp(zh-cn,MSDN.10).jpg"
                                            title="">
                                    </a>
                                </td>
                                <td align="center">
                                    <a href="http://www.cnblogs.com/gnielee/" target="_blank">
                                        <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_ljr(zh-cn,MSDN.10).jpg"
                                            title="">
                                    </a>
                                </td>
                                <td align="center">
                                    <a href="http://www.cnblogs.com/yungboy/archive/2010/08.html" target="_blank">
                                        <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_jjh(zh-cn,MSDN.10).jpg"
                                            title="">
                                    </a>
                                </td>
                                <td align="center">
                                    <a href="http://www.cnblogs.com/procoder" target="_blank">
                                        <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_lyj(zh-cn,MSDN.10).jpg"
                                            title="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" height="30">
                                    <a href="http://supper3000.cnblogs.com/" target="_blank">苏鹏 </a>
                                </td>
                                <td align="center" height="30">
                                    <a href="http://www.cnblogs.com/gnielee/" target="_blank">李敬然 </a>
                                </td>
                                <td align="center" height="30">
                                    <a href="http://www.cnblogs.com/yungboy/archive/2010/08.html" target="_blank">蒋建华
                                    </a>
                                </td>
                                <td align="center" height="30">
                                    <a href="http://www.cnblogs.com/procoder" target="_blank">林永坚 </a>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <a href="http://www.cnblogs.com/frank_xl/" target="_blank">
                                        <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_xl(zh-cn,MSDN.10).jpg"
                                            title=""></a>
                                </td>
                                <td align="center">
                                    <a href="http://www.cnblogs.com/WilsonWu/" target="_blank">
                                        <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_whf(zh-cn,MSDN.10).jpg"
                                            title=""></a>
                                </td>
                                <td align="center">
                                    <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_ymz(zh-cn,MSDN.10).jpg"
                                        title="">
                                </td>
                                <td align="center">
                                    <a href="http://www.cnblogs.com/chsword/" target="_blank">
                                        <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_zj(zh-cn,MSDN.10).jpg"
                                            title=""></a>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" height="30">
                                    <a href="http://www.cnblogs.com/frank_xl/" target="_blank">徐雷</a>
                                </td>
                                <td align="center" height="30">
                                    <a href="http://www.cnblogs.com/WilsonWu/" target="_blank">吴慧峰</a>
                                </td>
                                <td align="center" height="30">
                                    衣明志
                                </td>
                                <td align="center" height="30">
                                    <a href="http://www.cnblogs.com/chsword/" target="_blank">邹健</a>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <a href="http://xhinker.com" target="_blank">
                                        <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_zsd(zh-cn,MSDN.10).jpg"
                                            title=""></a>
                                </td>
                                <td align="center">
                                    <a href="http://www.cnblogs.com/xiaoyin_net/MyPosts.html?page=1" target="_blank">
                                        <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_gy(zh-cn,MSDN.10).jpg"
                                            title=""></a>
                                </td>
                                <td align="center">
                                    <a href="http://www.cnblogs.com/yilinglai" target="_blank">
                                        <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_lyl(zh-cn,MSDN.10).jpg"
                                            title=""></a>
                                </td>
                                <td align="center">
                                    <a href="http://zhaojunqi.cnblogs.com/" target="_blank">
                                        <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_zjq(zh-cn,MSDN.10).jpg"
                                            title=""></a>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" height="30">
                                    <a href="http://xhinker.com" target="_blank">朱书栋</a>
                                </td>
                                <td align="center" height="30">
                                    <a href="http://www.cnblogs.com/xiaoyin_net/MyPosts.html?page=1" target="_blank">高阳</a>
                                </td>
                                <td align="center" height="30">
                                    <a href="http://www.cnblogs.com/yilinglai" target="_blank">赖仪灵</a>
                                </td>
                                <td align="center" height="30">
                                    <a href="http://zhaojunqi.cnblogs.com/" target="_blank">赵俊其</a>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_cj(zh-cn,MSDN.10).jpg"
                                        title="">
                                </td>
                                <td align="center">
                                    <a href="http://jillzhang.cnblogs.com/" target="_blank">
                                        <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_zyb(zh-cn,MSDN.10).jpg"
                                            title="">
                                    </a>
                                </td>
                                <td align="center">
                                    &nbsp;
                                    <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_hy(zh-cn,MSDN.10).jpg"
                                        title="">
                                </td>
                                <td align="center">
                                    &nbsp; <a href="http://www.cnblogs.com/justben/" target="_blank">
                                        <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_xcl(zh-cn,MSDN.10).jpg">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" height="30">
                                    陈吉
                                </td>
                                <td align="center" height="30">
                                    <a href="http://jillzhang.cnblogs.com/" target="_blank">张玉彬 </a>
                                </td>
                                <td align="center">
                                    &nbsp;胡砚
                                </td>
                                <td align="center" height="30">
                                    &nbsp; <a href="http://www.cnblogs.com/justben/" target="_blank">徐长龙 </a>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <a href="http://randylee.cnblogs.com/" target="_blank">
                                        <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_lz(zh-cn,MSDN.10).jpg"
                                            title=""></a>
                                </td>
                                <td align="center">
                                    <a href="http://axzxs.blog.51cto.com/" target="_blank">
                                        <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_gsw(zh-cn,MSDN.10).jpg"
                                            title=""></a>
                                </td>
                                <td align="center">
                                    <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_lp(zh-cn,MSDN.10).jpg"
                                        title="">
                                </td>
                                <td align="center">
                                    <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_jyt(zh-cn,MSDN.10).jpg"
                                        title="">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" height="30">
                                    <a href="http://randylee.cnblogs.com/" target="_blank">李振</a>
                                </td>
                                <td align="center" height="30">
                                    <a href="http://axzxs.blog.51cto.com/" target="_blank">桂素伟</a>
                                </td>
                                <td align="center" height="30">
                                    李鹏
                                </td>
                                <td align="center">
                                    姜永涛
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_zq(zh-cn,MSDN.10).jpg"
                                        title="">
                                </td>
                                <td align="center">
                                    <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_ljz(zh-cn,MSDN.10).jpg"
                                        title="">
                                </td>
                                <td align="center">
                                    <a href="http://www.cnblogs.com/junxian_chen/" target="_blank">
                                        <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_cjx(zh-cn,MSDN.10).jpg"
                                            title=""></a>
                                </td>
                                <td align="center" height="30">
                                    &nbsp; <a href="http://www.cnblogs.com/bandik/" target="_blank">
                                        <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_fdx(zh-cn,MSDN.10).jpg"
                                            title=""></a>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" height="30">
                                    张权
                                </td>
                                <td align="center">
                                    李建忠
                                </td>
                                <td align="center">
                                    <a href="http://www.cnblogs.com/junxian_chen/" target="_blank">陈俊先</a>
                                </td>
                                <td align="center" height="30">
                                    &nbsp; <a href="http://www.cnblogs.com/bandik/" target="_blank">冯德旭</a>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <a href="http://www.cnblogs.com/DotNetNuke/" target="_blank">
                                        <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_ld(zh-cn,MSDN.10).jpg"
                                            title=""></a>
                                </td>
                                <td align="center">
                                    <a href="http://www.cnblogs.com/wengyuli/" target="_blank">
                                        <img align="top" alt="" src="http://i.msdn.microsoft.com/dd796167.teacher_wyl(zh-cn,MSDN.10).jpg"
                                            title=""></a>
                                </td>
                                <td align="center" height="30">
                                    &nbsp;
                                </td>
                                <td align="center" height="30">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <a href="http://www.cnblogs.com/DotNetNuke/" target="_blank">陆地</a>
                                </td>
                                <td align="center">
                                    <a href="http://www.cnblogs.com/wengyuli/" target="_blank">翁玉礼</a>
                                </td>
                                <td align="center" height="30">
                                    &nbsp;
                                </td>
                                <td align="center" height="30">
                                    &nbsp;
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /container -->
    <a id="scrollUp" href="#top" title="" style="position: fixed; z-index: 2147483647;
                                          display: block;"></a>
</body>
</html>
