﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/BlogManage/BlogManage.Master"
    Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link href="../../Scripts/datatable-1.9.4/media/css/demo_page.css" rel="stylesheet"
        type="text/css" />
    <script src="../../Scripts/datatable-1.9.4/media/js/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../Scripts/zwjs.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery/jquery.form.js" type="text/javascript"></script>
    <script src="../../Scripts/Lib/BlogManage/BlogSet.js" type="text/javascript"></script>
    <style>
        .block-content
        {
            border-radius: 0px 0px 3px 3px;
        }
        .block-header
        {
            margin: 0 2px;
            margin-bottom: -2px;
        }
        .block-border
        {
            width: 30%;
        }
     
        form label
        {
            float: left;
            width: 100px;
            text-align: center;
            font-size: 12px;
            font-weight: 700;
            color: #666 !important;
            text-shadow: 0 1px 0 #fff;
            vertical-align: middle;
        }
        form input, select
        {
            margin-left: 20px;
            width: 70%;
        }
        .block-actions
        {
            margin: 0 -10px -9px -10px;
            height: 44px;
        }
        input.text, input.email, input.password, textarea.uniform
        {
            font-size: 12px;
            font-family: "Helvetica Neue" ,Helvetica,Arial,sans-serif;
            font-weight: normal;
            padding: 3px 5px;
            color: #777;
            border-top: solid 1px #aaa;
            border-left: solid 1px #aaa;
            border-bottom: solid 1px #ccc;
            border-right: solid 1px #ccc;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            outline: 0;
        }
        #blogform > p {
            margin: 0;
            margin-top: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="breadcrumb">
        <ul id="breadcrumbs">
            <li><a href="/BlogManage/index" title="主页"><span id="bc-home"></span></a></li>
            <li>博客配置</li>
        </ul>
        <div class="shadow-bottom">
        </div>
    </div>
    <div id="main-content">
        <div class="header">
            <h1 class="page-title">
                博客配置</h1>
            <p style="font-size: 10px;">
                在这里配置您的博客</p>
        </div>
        <div class="block-border">
            <div class="block-header">
                <h1>
                    博客配置
                </h1>
                <span></span>
            </div>
            <div class="block-content">
                <form id="blogform" class="block-content form" action="/Manage/AddTitle" method="post">
                <input type="hidden" id="id" name="id" />
                <p>
                    <label for="title">
                        大标题
                    </label>
                    <input type="text" id="bigTitle" name="bigTitle" class="required text">
                </p>
                <p>
                    <label for="title">
                        小标题
                    </label>
                    <input type="text" id="smallTitle" name="smallTitle" class="required text">
                </p>
                <p>
                    <label for="title">
                        博客模板
                    </label>
                    <select name="template" id="template">
                        <option value="../../Scripts/Lib/Blog/Blog.css">默认</option>
                        <option value="../../Scripts/Lib/Blog/Blog1.css">版本一</option>
                        <option value="../../Scripts/Lib/Blog/Blog2.css">版本二</option>
                        <option value="../../Scripts/Lib/Blog/Blog3.css">版本三</option>
                        <option value="../../Scripts/Lib/Blog/Blog4.css">版本四</option>
                        <option value="../../Scripts/Lib/Blog/Blog5.css">版本五</option>
                    </select>
                </p>
                <div class="clear">
                </div>
                <div class="block-actions">
                    <ul class="actions-left">
                        <li></li>
                    </ul>
                    <ul class="actions-right">
                        <li>
                            <input type="submit" class="button" value="保存">
                        </li>
                    </ul>
                </div>
                </form>
            </div>
        </div>
    </div>
</asp:Content>
