﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/BlogManage/BlogManage.Master"
    Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/Lib/BlogManage/KeyManage.js" type="text/javascript"></script>
    <style>
        .block-border
        {
            width: 24%;
            float: left;
        }
        #key-form p label
        {
            float: left;
            display: block;
            padding: 4px 0 0 4px;
            margin-right: 20px;
            width: 80px;
            padding-left: 20px;
            font-size: 12px;
            font-weight: 700;
            color: #666 !important;
            text-shadow: 0 1px 0 #fff;
        }
        p
        {
            padding-right: 10px;
        }
        input.text, input.email, input.password, textarea.uniform
        {
            font-size: 12px;
            font-family: "Helvetica Neue" ,Helvetica,Arial,sans-serif;
            font-weight: normal;
            padding: 3px 0;
            color: #777;
            border-top: solid 1px #aaa;
            border-left: solid 1px #aaa;
            border-bottom: solid 1px #ccc;
            border-right: solid 1px #ccc;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            outline: 0;
            width: 65%;
            padding-left: 5px;
        }
        .inline-small-label
        {
            margin: 12px 0;
        }
        .block-actions
        {
            margin: 0 -9px -9px -9px;
            height: 44px;
        }
        .alert
        {
            background-image: url("../../images/cross-circle.png");
            background-color: #facfcf;
            border-color: #dc1c1c;
            border-left: none;
            border-right: none;
            color: #820101;
            border-radius: 0;
            margin: 0;
        }
        .error
        {
            border-color: #bf0000 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="breadcrumb">
        <ul id="breadcrumbs">
            <li><a href="/BlogManage/index" title="主页"><span id="bc-home"></span></a></li>
            <li>修改密码</li>
        </ul>
        <div class="shadow-bottom">
        </div>
    </div>
    <div id="main-content">
        <div class="header">
            <h1 class="page-title">
                修改密码</h1>
            <p style="font-size: 10px;">
                在这里修改您的密码</p>
        </div>
        <div class="block-border">
            <div class="block-header">
                <h1>
                    修改密码
                </h1>
                <span></span>
            </div>
            <form id="key-form" class="block-content form" action="" method="post">
            <div id="alertBox-generated" class="alert error no-margin top">
            </div>
            <p class="inline-small-label">
                <label for="username">
                    用户名
                </label>
                <input type="text" name="username" value="<%= HttpContext.Current.Session["username"] %>"
                    class="required text" readonly="readonly">
            </p>
            <p class="inline-small-label">
                <label for="password">
                    原密码
                </label>
                <input type="password" id="oldpassword" onkeyup="password_change(this);" name="oldpassword"
                    class="password">
            </p>
            <p class="inline-small-label">
                <label for="password">
                    新密码
                </label>
                <input type="password" id="newpassword1" onkeyup="password_change(this);" name="newpassword1"
                    class="required password">
            </p>
            <p class="inline-small-label">
                <label for="password">
                    请确认
                </label>
                <input type="password" id="newpassword2" onkeyup="password_change(this);" name="newpassword2"
                    class="required password">
            </p>
            <div class="clear">
            </div>
            <div class="block-actions">
                <ul class="actions-left">
                    <li><a class="button" name="recover_password" href="javascript:$('#key-form')[0].reset();">
                        重置密码 </a></li>
                    <li class="divider-vertical"></li>
                    <li><a class="button red" id="reset-login" href="javascript:void(0);">取消 </a></li>
                </ul>
                <ul class="actions-right">
                    <li><a class="button" id="A1" href="javascript:btn_updatekey();">保存 </a></li>
                </ul>
            </div>
            </form>
        </div>
       </div>
</asp:Content>
