﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/BlogManage/BlogManage.Master"
    Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link href="../../Scripts/datatable-1.9.4/media/css/demo_page.css" rel="stylesheet"
        type="text/css" />
    <script src="../../Scripts/datatable-1.9.4/media/js/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../Scripts/zwjs.js" type="text/javascript"></script>
    <script src="../../Scripts/Lib/BlogManage/ArticleManage.js" type="text/javascript"></script>
    <style>
        a:hover
        {
            color: inherit;
        }
       
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="breadcrumb">
        <ul id="breadcrumbs">
            <li><a href="/BlogManage/index" title="主页"><span id="bc-home"></span></a></li>
            <li>文章管理</li>
        </ul>
        <div class="shadow-bottom">
        </div>
    </div>
    <div id="main-content">
        <div class="header">
            <h1 class="page-title">
                文章管理</h1>
            <p style="font-size: 10px;">
                在这里管理您的文章</p>
        </div>
        <%
            if (ViewData["action"] != null)
            {
                var action = ViewData["action"].ToString();
                switch (action)
                {
                    case "del":
                        Response.Write("<div class='alert success'><strong>删除完成 </strong></div>");
                        break;
                    case "new":
                        Response.Write("<div class='alert success'><strong>发布成功 </strong></div>");
                        break;
                    case "edit":
                        Response.Write("<div class='alert success'><strong>编辑完成 </strong></div>");
                        break;
                    default:
                        break;
                }
            }
        %>
        <div class="block-border">
            <div class="block-header">
                <h1>
                    管理文章
                </h1>
                <span></span>
            </div>
            <div class="block-content">
                <div class="dataTables_wrapper" id="table-example_wrapper">
                    <table id="table-example" class="table">
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
