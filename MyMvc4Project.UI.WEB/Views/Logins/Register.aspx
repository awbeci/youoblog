﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta charset="utf-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>注册</title>
    <!--Bootstrap css-->
    <link rel="shortcut icon" href="../../Scripts/bootstrap-3.1.0/dist/ico/favicon.ico" />
    <link href="../../Scripts/bootstrap-3.1.0/dist/css/bootstrap.css" rel="stylesheet"
        type="text/css" />
    <link href="../../Scripts/Lib/Logins/signin.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery/jquery-1.8.3.js" type="text/javascript"></script>
     <!--jquery form js-->
    <script src="../../Scripts/jquery/jquery.form.js" type="text/javascript"></script>
    <!--bootstrap js-->
    <script src="../../Scripts/bootstrap-3.1.0/dist/js/bootstrap.js" type="text/javascript"></script>
    <script src="../../Scripts/HtmlHelper.js" type="text/javascript"></script>
    <script src="../../Scripts/zwjs.js" type="text/javascript"></script>
    <script src="../../Scripts/Lib/Logins/register.js" type="text/javascript"></script>
</head>
<body>
    <div class="container">
        <form id="form-register" class="form-signin" role="form" method="POST">
        <h2 class="form-signin-heading">
            欢迎注册</h2>
        <input type="text" name="username" class="form-control" placeholder="用户名" required autofocus>
        <input type="password" name="password" class="form-control" placeholder="密码" required>
        <label class="checkbox">
            <input type="checkbox" name="remeberme" value="remember-me">
            记住密码
        </label>
        <button class="btn btn-lg btn-primary btn-block" type="submit" onclick="register()">
            注册</button>
              <button class="btn btn-lg btn-primary btn-block" type="submit" onclick="quit()">
            返回</button>
        </form>
    </div>
</body>
</html>
