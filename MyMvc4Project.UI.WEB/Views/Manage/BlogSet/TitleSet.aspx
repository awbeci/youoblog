﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>标题设置</title>
     <link href="../../../Scripts/extjs/resources/css/ext-all-neptune.css" rel="stylesheet"
        type="text/css" />
    <link href="../../../Scripts/Lib/Manage/Manage.css" rel="stylesheet" type="text/css" />
    <script src="../../../Scripts/extjs/ext-all.js" type="text/javascript"></script>
    <script src="../../../Scripts/extjs/ext-theme-neptune.js" type="text/javascript"></script>
    <script src="../../../Scripts/extjs/locale/ext-lang-zh_CN.js" type="text/javascript"></script>
    <script src="../../../Scripts/HtmlHelper.js" type="text/javascript"></script>
    <script src="../../../Scripts/Lib/Manage/BlogSet/TitleSet.js" type="text/javascript"></script>
</head>
<body>
    <div id="titleSetDiv" style="margin:50px 300px;"></div>
</body>
</html>
