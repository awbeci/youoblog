﻿<%@ Page Language="C#" EnableEventValidation="false" ValidateRequest="false" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Import Namespace="MyMvc4Project.Dal.Views" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>NewArticle</title>
    <link href="../../Scripts/kindeditor-4.1.10/themes/default/default.css" rel="stylesheet"
        type="text/css" />
    <link href="../../Scripts/kindeditor-4.1.10/plugins/code/prettify.css" rel="stylesheet"
        type="text/css" />
    <link href="../../Scripts/Lib/Manage/Manage.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery/jquery.form.js" type="text/javascript"></script>
    <script src="../../Scripts/kindeditor-4.1.10/kindeditor-all-min.js" type="text/javascript"></script>
    <script src="../../Scripts/kindeditor-4.1.10/lang/zh_CN.js" type="text/javascript"></script>
    <script src="../../Scripts/kindeditor-4.1.10/plugins/code/code.js" type="text/javascript"></script>
    <script src="../../Scripts/kindeditor-4.1.10/plugins/code/prettify.js" type="text/javascript"></script>
   <script src="../../Scripts/zwjs.js" type="text/javascript"></script>
    <script src="../../Scripts/Lib/Manage/NewArticle.js" type="text/javascript"></script>
</head>
<body>
    <div class="kedit">
        <form id="form-article" name="form-article" method="POST">
        <p>
            标题：</p>
        <input type="text" id="txt_title" name="title" />
        <p>
            正文：</p>
        <textarea id="content-js" name="content"></textarea>
        <p>
            <% var lable = ViewData["lable"] as IList<LableView>;
               if (lable != null && lable.Count != 0)
               {
                   %>
                   <p>标签：</p>
                   <%
                   foreach (var view in lable)
                   {%>
            <input class="chk-lable" name="lable" type="checkbox" value="<%= view.Name %>" ><%= view.Name %></input>
            <%} %>
            <% }
               else
               { %>
               <p>无标签</p>
            <% } %>
        </p>
        <p>
            <input id="btn-submit" type="submit" value="发布" onclick="btn_submit()" />
            <input id="btn-cancel" type="button" value="取消" /></p>
        </form>
    </div>
</body>
</html>
