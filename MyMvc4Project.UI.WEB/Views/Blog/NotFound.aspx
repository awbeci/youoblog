﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>NotFound</title>
</head>
<body>
    <div>
        <h1>您查找的页面不存在！</h1>
    </div>
</body>
</html>
