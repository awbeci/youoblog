﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<%@ Import Namespace="MyMvc4Project.Dal.Views" %>
<%@ Import Namespace="MyMvc4Project.Models" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>优尔博客</title>
    <!--Bootstrap css-->
    <link rel="shortcut icon" href="../../Scripts/bootstrap-3.1.0/dist/ico/favicon.ico" />
    <link href="../../Scripts/bootstrap-3.1.0/dist/css/bootstrap.css" rel="stylesheet"
        type="text/css" />
    <link href="../../Scripts/bootstrap-3.1.0/jumbotron/jumbotron.css" rel="stylesheet"
        type="text/css" />
    <!--本页面 css-->
    <% var userdata = ViewData["Data"] as User;
       if (userdata != null)
       {%>
    <link href="<%= userdata.Blog.BlogCss %>" rel="stylesheet" type="text/css" />
    <% }
       else
       {%>
    <link href="../../Scripts/Lib/Blog/Blog.css" rel="stylesheet" type="text/css" />
    <%}%>
    <link href="../../Scripts/Lib/Blog/Article.css" rel="stylesheet" type="text/css" />
    <!--yg-->
    <link href="../../Scripts/syntaxhighlighter_3.0.83/styles/shCoreDefault.css" rel="stylesheet"
        type="text/css" />
    <script src="../../Scripts/jquery/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery/jquery.form.js" type="text/javascript"></script>
    <!--jquery-scrollUp js-->
    <script src="../../Scripts/jquery.scrollUp.min.js" type="text/javascript"></script>
    <!--bootstrap js-->
    <script src="../../Scripts/bootstrap-3.1.0/dist/js/bootstrap.js" type="text/javascript"></script>
    <script src="../../Scripts/HtmlHelper.js" type="text/javascript"></script>
    <script src="../../Scripts/zwjs.js" type="text/javascript"></script>
    <!--yg-->
    <script src="../../Scripts/syntaxhighlighter_3.0.83/scripts/shCore.js" type="text/javascript"></script>
    <script src="../../Scripts/syntaxhighlighter_3.0.83/scripts/shBrushCSharp.js" type="text/javascript"></script>
    <script src="../../Scripts/syntaxhighlighter_3.0.83/scripts/shBrushJScript.js" type="text/javascript"></script>
    <script src="../../Scripts/syntaxhighlighter_3.0.83/scripts/shBrushCss.js" type="text/javascript"></script>
    <script src="../../Scripts/syntaxhighlighter_3.0.83/scripts/shBrushJava.js" type="text/javascript"></script>
    <script src="../../Scripts/syntaxhighlighter_3.0.83/scripts/shBrushSql.js" type="text/javascript"></script>
    <script src="../../Scripts/syntaxhighlighter_3.0.83/scripts/shBrushXml.js" type="text/javascript"></script>
    <!--本页面 js-->
    <script src="../../Scripts/Lib/Blog/Article.js" type="text/javascript"></script>
    <style>
        .tag
        {
            border-bottom: 1px solid #000;
            padding-bottom: 5px;
        }
        .action
        {
            float: right;
            padding-top: 5px;
        }
        .action a
        {
            padding-left: 3px;
        }
    </style>
</head>
<body>
    <%var comment = ViewData["comment"] as IList<Comment>;
      var name = ViewData["sessionname"].ToString();
    %>
    <div class="container">
        <div class="blog-header">
            <% if (userdata != null)
               {%>
            <h1 class="blog-title">
                <%= userdata.Blog.BigTitle%></h1>
            <p class="lead blog-description">
                <%= userdata.Blog.SmallTitle%></p>
            <% }
               else
               {%>
            <h1 class="blog-title">
            </h1>
            <p class="lead blog-description">
            </p>
            <%}%>
        </div>
        <div class="row">
            <div class="col-sm-9 blog-article">
                <!-- /.blog-post -->
                <div class="blog-post">
                    <h3 class="blog-post-title">
                        <%= Html.Encode(Model.Title) %>
                    </h3>
                    <p class="blog-post-meta">
                        <%= Model.CreateTime.ToString("yyyy年MM月dd日 H时m分")%>
                        | <a href="/Blog/Index/<%= Html.Encode(Model.User.Name) %>">
                            <%= Html.Encode(Model.User.Name)%></a>
                    </p>
                    <p style="line-height: 2em;">
                        <%= HttpContext.Current.Server.UrlDecode(Model.Contents)%></p>
                    <p>
                        &nbsp;</p>
                    <div class="tag">
                        Tags：
                        <% foreach (var view in Model.Lables)
                           {%>
                        <a href="/Blog/ArticleList?name=<%= Html.Encode(Model.User.Name)%>&para=<%= Html.Encode(view.Name)%>&type=lb">
                            <%=view.Name %>
                        </a>
                        <%} %>
                    </div>
                    <div class="action">
                        <% var username = HttpContext.Current.Session["username"];
                           if (username == null)
                           {%>
                        <a href="/Logins/Login">编辑</a>
                        <%}
                           else
                           {
                               if (username.ToString() == Model.User.Name)
                               {%>
                        <a href="/BlogManage/EditArticle?id=<%= Html.Encode(Model.Id) %>">编辑</a>
                        <%}
                               else
                               {%>
                        <a href="/BlogManage/ArticleManage">编辑</a>
                        <%} %>
                        <%} %>
                        <a href="javascript:void(0)">阅读(<%= Html.Encode(Model.ClickCount)%>)</a> <a href="javascript:void(0)">
                            评论(<% if (comment == null)
                                  {
                                      Response.Write(0);
                                  }
                                  else
                                  {
                                      Response.Write(comment.Count);
                                  }
                            %>)</a>
                    </div>
                </div>
                <div class="comment">
                    <% if (!string.IsNullOrEmpty(name))
                       {%>
                    <p>
                        发表评论</p>
                    <div class="comment-form">
                        <form id="form-comment" class="form-comment" role="form" method="POST">
                        <p>
                            用户名</p>
                        <input type="text" id="name" name="name" class="form-control" required value="<%=ViewData["sessionname"] %>" />
                        <p>
                            评论内容</p>
                        <textarea id="contents" name="contents" rows="2" cols="20" class="form-control" required></textarea>
                        <button id="btn-comment" class="btn btn-primary" type="submit" onclick="btn_submit('<%= Html.Encode(Model.Id) %>')">
                            评论</button>
                        </form>
                    </div>
                    <%}
                       else
                       {%>
                    <p>
                        请先登入再评论！
                        <button class="btn btn-xs btn-primary" onclick="btn_login()">
                            登入</button></p>
                    <%} %>
                    <div class="comment-contents">
                        <% if (comment != null && comment.Count != 0)
                           {%>
                        <p>
                            评论</p>
                        <div class="cc-div">
                            <%foreach (var view in comment)
                              { %>
                            <div class="cc-comments">
                                <div class="cc-top">
                                    <img src="<%= view.User.Avatar %>" height="32" width="32" />
                                    <span>#&nbsp;<a href="/Blog/Index/<%= view.User.Name %>"><%= view.User.Name %></a></span>
                                    <span>
                                        <%= view.CommentTime.GetValueOrDefault().ToString("yyyy年MM月dd日 H时m分")%></span>
                                </div>
                                <div class="cc-bottom">
                                    <p>
                                        <%= view.Contents %></p>
                                </div>
                            </div>
                            <% }%>
                        </div>
                        <%}
                           else
                           {%>
                        <p>
                            无评论</p>
                        <%} %>
                    </div>
                </div>
                <footer><p>© 合肥优尔电子科技有限公司 2014</p></footer>
            </div>
            <!-- /.blog-main -->
            <div class="col-sm-3 col-sm-offset-0 blog-sidebar">
                <div class="sidebar-module">
                    <h4>
                        导航</h4>
                    <ol class="list-unstyled">
                        <li><a href="/home">首页</a></li>
                        <li><a onclick="userLogin()" href="#">个人主页</a></li>
                        <li><a href="/BlogManage/NewArticle">新文章</a></li>
                        <li><a href="/BlogManage/ArticleManage">管理</a></li>
                        <li><a href="#">关于</a></li>
                    </ol>
                </div>
                <div class="sidebar-module">
                    <h4>
                        标签</h4>
                    <ol class="list-unstyled list-tag">
                    </ol>
                </div>
                <div class="sidebar-module">
                    <h4>
                        档案</h4>
                    <ol class="list-unstyled list-archives">
                    </ol>
                </div>
                <div class="sidebar-module">
                    <h4>
                        联系我</h4>
                    <ol class="list-unstyled">
                        <li><a href="#">GitHub</a></li>
                        <li><a href="#">Twitter</a></li>
                        <li><a href="#">Facebook</a></li>
                    </ol>
                </div>
            </div>
            <!-- /.blog-sidebar -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
    <%-- <div class="blog-footer">
      <p>博客模板制作 <a href="http://getbootstrap.com">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.</p>
      <p>
        <a href="#">回到顶部</a>
      </p>
    </div>--%>
    <!-- /container -->
    <a id="scrollUp" href="#top" title="" style="position: fixed; z-index: 2147483647;
        display: block;"></a>
</body>
</html>
