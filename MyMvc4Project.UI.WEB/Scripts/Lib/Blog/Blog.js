﻿$(function () {
    scrollUp();
    getAllArticle();
    getAllLable();
    getArchives();
});

var myCurrentPage = 1, myTotalPages = 0;

//------------------------------------------------------------------------------------------
function scrollUp() {
    $.scrollUp({
        scrollName: 'scrollUp', // Element ID
        topDistance: '300', // Distance from top before showing element (px)
        topSpeed: 300, // Speed back to top (ms)
        animation: 'fade', // Fade, slide, none
        animationInSpeed: 200, // Animation in speed (ms)
        animationOutSpeed: 200, // Animation out speed (ms)
        scrollText: '', // Text for element
        activeOverlay: false  // Set CSS color to display scrollUp active point, e.g '#00FFFF'
    });
}

function bootStrapPage() {
    var options = {
        currentPage: myCurrentPage,
        totalPages: myTotalPages,
        numberOfPages: 15,
        size: 'small',
        itemContainerClass: function (type, page, current) {
            return (page === current) ? "active" : "pointer-cursor";
        },
        itemTexts: function (type, page, current) {
            switch (type) {
                case "first":
                    return "First";
                case "prev":
                    return "上一页";
                case "next":
                    return "下一页";
                case "last":
                    return "Last";
                case "page":
                    return page;
                default:
                    return null;
            }
        },
        shouldShowPage: function (type, page, current) {
            switch (type) {
                case "first":
                case "last":
                    return false;
                default:
                    return true;
            }
        },
        onPageClicked: function (e, originalEvent, type, page) {
            myCurrentPage = page;
            zwobj.url = Resolve2('/Blog/GetAllArticle/?currentPage=' + myCurrentPage);
            ajaxData();
        }
    };
    $('#example').bootstrapPaginator(options);
}



function getAllArticle() {
    zwobj.url = Resolve2('/Blog/GetAllArticle/?currentPage=' + myCurrentPage);
    ajaxData();
}

function getAllLable() {
    zwobj.url = Resolve2('/Blog/MyGetAllByUserId');
    ajaxData();
}

function getArchives() {
    zwobj.url = Resolve2('/Blog/GetArchives');
    ajaxData();
}

//-----------------------------------------------------------------------------------------------

function userLogin() {
    zwobj.url = Resolve2('/Blog/LoginUserMainPage');
    ajaxData();
}

function newArticle() {
    location.href = "/BlogManage";
}


//------------------------------------------------------------------------------------------------
function ajax_GetAllArticle(data) {
    $(".blog-main").empty();
    var html = "";
    var len = data.Data.length;
    for (var i = 0; i < len; i++) {
        html += '<div class="blog-post">';
        html += '<h3 class="blog-post-title"><a href="/Blog/Article?name=' + data.Data[i].UserName + '&id=' + data.Data[i].Id + '">' + data.Data[i].Title + '</a></h3>';
        html += ' <p class="blog-post-meta"><a href="#">' + eval("new " + data.Data[i].CreateTime.split('/')[1]).Format("yyyy年MM月dd日 HH时mm分") +
            ' | ' + data.Data[i].UserName + '</a></p>';
        html += '<p>' + decodeURI(data.Data[i].Contents).replace(/<[^>].*?>/g, "").substr(0, 400) + '</p>';
        html += '<p>&nbsp;</p>';
        html += ' <div class="tag">Tags：';
        var len2 = data.Data[i].Lables.length;
        for (var j = 0; j < len2; j++) {
            html += '<a href="/Blog/ArticleList?name=' + data.Data[i].UserName + '&para=' +
                data.Data[i].Lables[j].Name + '&type=lb" rel="tag">' + data.Data[i].Lables[j].Name + '</a>&nbsp;&nbsp;';
        }
        html += '</div></div>';
    }
    html += ' <div id="example"></div>';
    html += '<footer><p>© 合肥优尔电子科技有限公司 2014</p></footer>';
    $(".blog-main").append(html);

    //总页数
    var total = parseInt(data.Other);
    myTotalPages = Math.ceil(total / 10);
    if (total != 0) {
        bootStrapPage();
    }

}


function ajax_GetUserLable(data) {
    $(".list-tag").empty();
    var html = "";
    var len = data.Data.length;
    for (var i = 0; i < len; i++) {
        html += '<li class="level-' + data.Data[i].Level +
            '"><a href="/Blog/ArticleList?name=' + data.Other + '&para=' + data.Data[i].Name + '&type=lb">' +
            data.Data[i].Name + '</a></li>';
    }
    $(".list-tag").append(html);
}

function ajax_GetArchives(data) {
    $(".list-archives").empty();
    var html = "";
    var len = data.Data.length;
    for (var i = 0; i < len; i++) {
        html += '<li><a href="/Blog/ArticleList?name=' + data.Other +
            '&para=' + data.Data[i][2] + '&type=dt">' + data.Data[i][0] + '(' + data.Data[i][1] + ')' + '</a></li>';
    }
    $(".list-archives").append(html);
}

function ajax_LoginUserMainPage(data) {
    location.href = data.Other;
}