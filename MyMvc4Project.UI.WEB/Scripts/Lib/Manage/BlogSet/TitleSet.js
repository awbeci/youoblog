﻿Ext.Loader.setConfig({
    enabled: true
});
Ext.Loader.setPath('Ext.ux', '../../../../scripts/extjs/examples/ux');

Ext.require([
//'Ext.form.*',
//'Ext.layout.container.Column',
//'Ext.tab.Panel'
    '*',
    'Ext.ux.DataTip'
]);
Ext.onReady(function () {
    Ext.QuickTips.init();
    Ext.Ajax.request({
        url: '/Manage/TitleData',
        async: false,
        success: function (response) {
            var obj = Ext.JSON.decode(response.responseText);
            var id = obj.id;
            var formWidget = Ext.widget({
                xtype: 'form',
                layout: 'form',
                renderTo: 'titleSetDiv',
                id: 'titleSetForm',
                frame: true,
                title: '博客设置',
                bodyPadding: '5 5 0',
                width: 350,
                fieldDefaults: {
                    msgTarget: 'side',
                    labelWidth: 75
                },
                plugins: {
                    ptype: 'datatip'
                },
                defaultType: 'textfield',
                items: [{
                    fieldLabel: '博客大标题',
                    name: 'bigTitle',
                    tooltip: '输入你的博客大标题',
                    value: obj.bigTitle
                }, {
                    fieldLabel: '博客小标题',
                    name: 'smallTitle',
                    tooltip: '输入你的博客小标题',
                    value: obj.smallTitle
                }, {
                    fieldLabel: '博客模板',
                    xtype: "combo",
                    id: 'template',
                    name: 'template',
                    store: states,
                    queryMode: 'local',
                    displayField: 'name',
                    valueField: 'value',
                    editable: false// 是否允许输入
                }],

                buttons: [{
                    text: '保存',
                    icon: '../../../Images/extjs/disk.png',
                    handler: function () {
                        formWidget.getForm().submit({
                            url: '/Manage/AddTitle',
                            params: {
                                id: id
                            },
                            method: 'POST',
                            success: function (data, action) {

                            },
                            failure: function (data, action) {
                                Ext.Msg.alert('Failed', action.result.msg);
                            }
                        });
                    }
                }, {
                    text: '取消',
                    icon: '../../../Images/extjs/cross.png',
                    handler: function () {
                        this.up('form').getForm().reset();
                    }
                }]
            });
            if (obj.blogCss == "") {
                Ext.getCmp('template').setValue('../../Scripts/Lib/Blog/Blog.css'); //设置combo默认为第一选项
            } else {
                Ext.getCmp('template').setValue(obj.blogCss);
            }
        },
        failure: function (response, opts) {
            var obj = Ext.JSON.decode(response.responseText);
            Ext.Msg.alert('Error', obj.msg);
        }
    });
});

//---------------------------------------------------------------------------------
var states = Ext.create('Ext.data.Store', {
    fields: ['name', 'value'],
    data: [
        { "name": "默认", "value": "../../Scripts/Lib/Blog/Blog.css" },
        { "name": "版本一", "value": "../../Scripts/Lib/Blog/Blog1.css" },
        { "name": "版本二", "value": "../../Scripts/Lib/Blog/Blog2.css" }
    ]
});