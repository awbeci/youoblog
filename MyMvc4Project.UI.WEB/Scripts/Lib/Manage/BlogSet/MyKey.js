﻿Ext.apply(Ext.form.VTypes, {
    password: function (val, field) {
        if (field.initialPassField) {
            var pwd = Ext.getCmp(field.initialPassField);
            return (val == pwd.getValue());
        }
        return true;
    },
    passwordText: '两次输入的密码不一致!'
});

Ext.Loader.setConfig({
    enabled: true
});
Ext.Loader.setPath('Ext.ux', '../../../../scripts/extjs/examples/ux');

Ext.require([
//'Ext.form.*',
//'Ext.layout.container.Column',
//'Ext.tab.Panel'
    '*',
    'Ext.ux.DataTip'
]);
Ext.onReady(function () {
    Ext.QuickTips.init();

    var formWidget = Ext.widget({
        xtype: 'form',
        layout: 'form',
        renderTo: 'myKeyDiv',
        id: 'titleSetForm',
        frame: true,
        title: '修改密码',
        bodyPadding: '5 5 0',
        width: 350,
        fieldDefaults: {
            msgTarget: 'side',
            labelWidth: 75
        },
        plugins: {
            ptype: 'datatip'
        },
        defaultType: 'textfield',
        items: [{
            fieldLabel: '当前密码',
            inputType: 'password',    //密码框属性设置
            name: 'keyold',
            allowBlank: false,
            blankText: '密码不能为空',
            regex: /^[\s\S]{0,20}$/,
            regexText: '密码长度不能超过20个字符'
        }, {
            id: 'keynew1',
            fieldLabel: '新密码',
            inputType: 'password',    //密码框属性设置
            name: 'keynew1',
            allowBlank: false,
            blankText: '密码不能为空',
            regex: /^[\s\S]{0,20}$/,
            regexText: '密码长度不能超过20个字符'
        }, {
            fieldLabel: '确认密码',
            inputType: 'password',    //密码框属性设置
            initialPassField: 'keynew1',
            id: 'keynew2',
            name: 'keynew2',
            vtype: 'password',
            allowBlank: false,
            blankText: '密码不能为空',
            regex: /^[\s\S]{0,20}$/,
            regexText: '密码长度不能超过20个字符'
        }],

        buttons: [{
            text: '保存',
            icon: '../../../Images/extjs/disk.png',
            handler: function () {
                if (!formWidget.getForm().isValid()) {
                    return;
                }
                formWidget.getForm().submit({
                    url: '/Manage/UpdateKey',
                    method: 'POST',
                    success: function (data, action) {
                        Ext.MessageBox.show({
                            title: '提示',
                            msg: action.result.msg,
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.INFO
                        });
                        parent.location.href = '/Home';
                    },
                    failure: function (data, action) {
                        Ext.MessageBox.show({
                            title: '提示',
                            msg: action.result.msg,
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.INFO
                        });
                    }
                });
            }
        }, {
            text: '取消',
            icon: '../../../Images/extjs/cross.png',
            handler: function () {
                this.up('form').getForm().reset();
            }
        }]
    });
});