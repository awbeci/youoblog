﻿var itemsPerPage = 10;   // set the number of items you want per page

//extjs初始化
Ext.onReady(function () {
    gridPanel();

    //调整gridpanel根据屏幕分辨率来显示
    var panel = Ext.getCmp('gridPanel');
    window.onresize = function () {
        panel.setHeight(document.documentElement.clientHeight);
    };
});

//----------------------------------------------------------------------------------

function gridPanel() {
    var panel = new Ext.container.Viewport({
        items: {
            xtype: 'gridpanel',
            id: 'gridPanel',
            store: store,
            height: document.documentElement.clientHeight,
            columns: [
            { header: "Id", hidden: true, flex: 1, sortable: false, dataIndex: 'Id' },
            { header: "标签名", width: 250, sortable: false, dataIndex: 'Name' },
            { header: "等级分配", width: 80, align: 'center', sortable: false, dataIndex: 'Level' },
            { header: "创建时间", width: 150, align: 'center', sortable: false, dataIndex: 'CreateTime',
                renderer: function (value) {
                    return eval("new " + value.split('/')[1]).Format("yyyy-MM-dd HH:mm:ss");
                }
            }
            ],
            loadMask: true,
            viewConfig: {
                stripeRows: true
            },
            tbar: ['->',
                { id: 'btn_add', text: '添加', icon: '../../../Images/extjs/add.png', handler: btnAdd },
                { id: 'btn_edit', text: '编辑', icon: '../../../Images/extjs/pencil.png', handler: btnEdit },
                { id: 'btn_delete', text: '删除', icon: '../../../Images/extjs/delete.png', handler: btnDelete }
            ],
            dockedItems: [{
                xtype: 'pagingtoolbar',
                store: store,
                dock: 'bottom',
                displayMsg: '显示 {0} - {1} ，共 {2} 记录',
                displayInfo: true
            }]
        }
    });
}

//grid数据源
var store = Ext.create('Ext.data.Store', {
    pageSize: itemsPerPage,
    fields: ['Id', 'Name', 'Level', 'CreateTime'],
    remoteSort: true,
    proxy: {
        type: 'ajax',
        url: '/Manage/LabelData',
        reader: {
            type: 'json',
            root: 'topics',
            totalProperty: 'totalCount'
        }
    }
});
//grid数据源上传
store.reload({ params: { start: 0, limit: itemsPerPage} });

//-------------------------------------------------------------------------------------------------
function btnAdd() {
    //点击编辑弹出的formpanel
    var formPanel = new Ext.FormPanel({
        bodyStyle: 'padding:5px 20px 0 5px',
        autoScroll: true,
        layout: 'form',
        items: [{
            fieldLabel: '标签名',
            xtype: 'textfield',
            name: 'name',
            allowBlank: false
        }, {
            fieldLabel: '标签等级',
            xtype: 'numberfield',
            name: 'level',
            value: 1,
            maxValue: 10,
            minValue: 1
        }

          ],
        buttonAlign: 'center',
        buttons: [
        {
            text: '保存',
            icon: '../../../Images/extjs/disk.png',
            handler: function () {
                if (!formPanel.getForm().isValid()) {
                    return;
                }
                formPanel.getForm().submit({
                    url: '/Manage/AddLabel',
                    method: 'get',
                    success: function (form, action) {
                        if (action.result.success) {
                            win.close(this);
                            store.reload();
                        }
                    }, failure: function (form, action) {
                        Ext.MessageBox.show({
                            title: '提示',
                            msg: action.result.errors.info,
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.INFO
                        });
                        win.close(this);
                    }
                });
            }
        }, {
            text: '关闭',
            icon: '../../../Images/extjs/cross.png',
            handler: function () {
                win.close(this);
            }
        }
      ]
    });

    //点击编辑弹出的windows(formpanel作为window的items)
    var win = Ext.create("Ext.window.Window", {
        title: "添加",       //标题
        draggable: false,
        icon: '../../../Images/extjs/add.png',
        height: 200,                          //高度
        width: 350,                           //宽度
        layout: "fit",                        //窗口布局类型
        modal: true, //是否模态窗口，默认为false
        resizable: false,
        items: [formPanel]
    });
    win.show();
}

function btnEdit() {
    //获取grid选中的数据
    var sm = Ext.getCmp("gridPanel").getSelectionModel();
    var rows = sm.getSelection();
    if (!sm.hasSelection()) {
        Ext.MessageBox.show({
            title: '提示',
            msg: '请选择一行数据进行操作！',
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.INFO
        });
        return;
    }
    var id = rows[0].get('Id');

    //点击编辑弹出的formpanel
    var formPanel = new Ext.FormPanel({
        bodyStyle: 'padding:5px 20px 0 5px',
        autoScroll: true,
        layout: 'form',
        items: [{
            fieldLabel: '标签名',
            xtype: 'textfield',
            name: 'name',
            value: rows[0].get('Name'),
            allowBlank: false
        }, {
            fieldLabel: '标签等级',
            xtype: 'numberfield',
            name: 'level',
            maxValue: 10,
            minValue: 1,
            value: rows[0].get('Level')
        }

          ],
        buttonAlign: 'center',
        buttons: [
        {
            text: '保存',
            icon: '../../../Images/extjs/disk.png',
            handler: function () {
                if (!formPanel.getForm().isValid()) {
                    return;
                }
                Ext.Ajax.request({
                    url: '/Manage/EditLabel',
                    method: 'get',
                    params: {
                        id: id,
                        name: formPanel.getForm().findField("name").getValue(),
                        level: formPanel.getForm().findField("level").getValue()
                    },
                    success: function (response) {
                        var obj = Ext.JSON.decode(response.responseText);
                        if (obj.success) {
                            win.close(this);
                            store.reload();
                        }
                    },
                    failure: function (form, action) {
                        Ext.MessageBox.show({
                            title: '提示',
                            msg: action.result.errors.info,
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.INFO
                        });
                        win.close(this);
                    }
                });
            }
        }, {
            text: '关闭',
            icon: '../../../Images/extjs/cross.png',
            handler: function () {
                win.close(this);
            }
        }
      ]
    });

    //点击编辑弹出的windows(formpanel作为window的items)
    var win = Ext.create("Ext.window.Window", {
        title: "编辑",       //标题
        draggable: false,
        icon: '../../../Images/extjs/pencil.png',
        height: 200,                          //高度
        width: 350,                           //宽度
        layout: "fit",                        //窗口布局类型
        modal: true, //是否模态窗口，默认为false
        resizable: false,
        items: [formPanel]
    });
    win.show();
}

function btnDelete() {
    //获取grid选中的数据
    var sm = Ext.getCmp("gridPanel").getSelectionModel();
    var rows = sm.getSelection();
    if (!sm.hasSelection()) {
        Ext.MessageBox.show({
            title: '提示',
            msg: '请选择一行数据进行操作！',
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.INFO
        });
        return;
    }
    Ext.Msg.show({
        title: '删除',
        buttons: Ext.MessageBox.YESNO,
        msg: '确定删除这条数据?',
        icon: Ext.MessageBox.QUESTION,
        fn: function (btn) {
            if (btn == 'yes') {
                Ext.Ajax.request({
                    url: '/Manage/DeleteLabel',
                    params: {
                        id: rows[0].get('Id')
                    },
                    method: 'POST',
                    success: function (response) {
                        var obj = Ext.JSON.decode(response.responseText);
                        if (obj.success) {
                            store.reload();
                        }
                    },
                    failure: function (form, action) {
                        Ext.Msg.alert('Error', 'ajax提交失败，请联系管理员！');
                    }
                });
            }
        }
    });
}