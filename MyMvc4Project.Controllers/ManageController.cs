﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using MyMvc4Project.Dal.Views;
using MyMvc4Project.Models;
using NHibernate;
using System.Web.Mvc;
using MyMvc4Project.Infrastructure.Helpers;
using MyMvc4Project.Dal.Implementation;
using MyMvc4Project.Service;

namespace MyMvc4Project.Controllers
{
    public class ManageController : Controller
    {
        readonly string _userimgpath = ConfigurationManager.AppSettings["userimgpath"];
        public ActionResult BlogManage()
        {
            var user = System.Web.HttpContext.Current.Session["username"];
            if (user == null)
            {
                return Redirect("~/Logins/Login");
            }
            return View();
        }

        /// <summary>
        /// /Manage/ArticleManage
        /// </summary>
        /// <returns></returns>
        public ActionResult ArticleManage()
        {
            return View();
        }

        /// <summary>
        /// /Manage/LabelManage
        /// </summary>
        /// <returns></returns>
        public ActionResult LabelManage()
        {
            return View();
        }


        /// <summary>
        /// /Manage/UserManage
        /// </summary>
        /// <returns></returns>
        public ActionResult UserManage()
        {
            return View();
        }

        /// <summary>
        /// /Manage/NoticeManage
        /// </summary>
        /// <returns></returns>
        public ActionResult NoticeManage()
        {
            return View();
        }

        /// <summary>
        /// /Manage/TitleSet
        /// </summary>
        /// <returns></returns>
        public ActionResult TitleSet()
        {
            return View("BlogSet/TitleSet");
        }

        /// <summary>
        /// /Manage/MyKey
        /// </summary>
        /// <returns></returns>
        public ActionResult MyKey()
        {
            return View("BlogSet/MyKey");
        }

        //-------------------------------------------新文章开始-------------------------------------------------------
        /// <summary>
        /// /Manage/NewArticle
        /// </summary>
        /// <returns></returns>
        public ActionResult NewArticle()
        {
            var user = System.Web.HttpContext.Current.Session["username"];
            if (user == null)
            {
                return Redirect("~/Logins/Login");
            }
            LableDal lableDal = new LableDal();
            var data = lableDal.MyGetAllByUserId(user.ToString());
            ViewData["lable"] = data;
            return View();
        }

        /// <summary>
        /// /Manage/GetLableNameByUserId
        /// </summary>
        /// <returns></returns>
        public ActionResult GetLableNameByUserId()
        {
            var user = System.Web.HttpContext.Current.Session["username"];
            if (user == null)
            {
                return null;
            }
            LableDal lableDal = new LableDal();
            var data = lableDal.MyGetAllByUserId(user.ToString());
            var json = Json(new { data }, JsonRequestBehavior.AllowGet);
            return json;
        }

        [HttpPost]
        public JsonResult AddArticle()
        {
            ZwJson zwJson = new ZwJson();
            var user = System.Web.HttpContext.Current.Session["username"];
            UserDal userDal = new UserDal();
            if (user == null)
            {
                zwJson.IsSuccess = false;
                zwJson.Msg = "请登录系统";
                zwJson.Other = "/Logins/Login";
                return Json(zwJson, JsonRequestBehavior.AllowGet);
            }
            var users = userDal.GetByName(user.ToString());

            var title = System.Web.HttpContext.Current.Request["title"];
            var lable = System.Web.HttpContext.Current.Request["lables"];
            var content = System.Web.HttpContext.Current.Request["content"];

            var lables = lable.Split(',');
            IList<Lable> lbls = new List<Lable>();
            LableDal lableDal = new LableDal();
            foreach (var s in lables)
            {
                var lbl = lableDal.GetByName(s);
                lbls.Add(lbl);
            }
            ArticleDal articleDal = new ArticleDal();
            articleDal.Save(new Article
                {
                    Id = Guid.NewGuid().ToString(),
                    User = users,
                    Title = title,
                    Contents = content,
                    CreateTime = DateTime.Now,
                    UpdateTime = DateTime.Now,
                    Lables = lbls
                });
            zwJson.IsSuccess = true;
            zwJson.JsExecuteMethod = "ajax_AddArticle";
            return Json(zwJson, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveExtJsImg()
        {
            string savePath = "/UploadImages/";
            string saveUrl = "/UploadImages/";
            string fileTypes = "gif,jpg,jpeg,png,bmp";
            int maxSize = 1000000;

            Hashtable hash = new Hashtable();

            HttpPostedFile file = System.Web.HttpContext.Current.Request.Files["upload"];
            var width = System.Web.HttpContext.Current.Request.Params["width"];
            var height = System.Web.HttpContext.Current.Request.Params["height"];
            var content = System.Web.HttpContext.Current.Request.Params["content"];
            if (file == null)
            {
                hash = new Hashtable();
                hash["error"] = 1;
                hash["message"] = "请选择文件";
                return Json(hash, "text/html;charset=UTF-8");
            }

            string dirPath = System.Web.HttpContext.Current.Server.MapPath(savePath);
            if (!Directory.Exists(dirPath))
            {
                hash = new Hashtable();
                hash["error"] = 1;
                hash["message"] = "上传目录不存在";
                return Json(hash, "text/html;charset=UTF-8");
            }

            string fileName = file.FileName;
            string fileExt = Path.GetExtension(fileName).ToLower();

            ArrayList fileTypeList = ArrayList.Adapter(fileTypes.Split(','));

            if (file.InputStream == null || file.InputStream.Length > maxSize)
            {
                hash = new Hashtable();
                hash["error"] = 1;
                hash["message"] = "上传文件大小超过限制";
                return Json(hash, "text/html;charset=UTF-8");
            }

            if (string.IsNullOrEmpty(fileExt) || Array.IndexOf(fileTypes.Split(','), fileExt.Substring(1).ToLower()) == -1)
            {
                hash = new Hashtable();
                hash["error"] = 1;
                hash["message"] = "上传文件扩展名是不允许的扩展名";
                return Json(hash, "text/html;charset=UTF-8");
            }

            string newFileName = DateTime.Now.ToString("yyyyMMddHHmmss_ffff", DateTimeFormatInfo.InvariantInfo) + fileExt;
            string filePath = dirPath + newFileName;
            file.SaveAs(filePath);
            string fileUrl = saveUrl + newFileName;

            hash = new Hashtable();
            var data = new Hashtable();
            data["width"] = width;
            data["height"] = height;
            data["content"] = content;
            data["url"] = fileUrl;

            hash["success"] = true;
            hash["data"] = data;
            hash["msg"] = "上传成功";
            return Json(hash, "text/html;charset=UTF-8");
        }
        //-------------------------------------------新文章结束-------------------------------------------------------

        //-------------------------------------------文章管理开始-------------------------------------------------------

        /// <summary>
        /// 获取文章数据
        /// </summary>
        /// <param name="start"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public ActionResult ArticleData(string start, string limit)
        {
            ArticleDal articleDal = new ArticleDal();
            var user = System.Web.HttpContext.Current.Session["username"];
            if (user == null)
            {
                return Redirect("~/Logins/Login");
            }
            var total = 0;
            var data = articleDal.MyGetAll2(user.ToString(), int.Parse(start), int.Parse(limit), ref total);
            var json = Json(new { totalCount = total, topics = data }, JsonRequestBehavior.AllowGet);
            return json;
        }

        /// <summary>
        /// 保存文章
        /// </summary>
        /// <returns></returns>
        public ActionResult UpdateArticle()
        {
            var articleDal = new ArticleDal();
            var user = System.Web.HttpContext.Current.Session["username"];
            var userDal = new UserDal();
            if (user == null)
            {
                return Redirect("~/Logins/Login");
            }

            var users = userDal.GetByName(user.ToString());
            var id = System.Web.HttpContext.Current.Request.Params["id"];
            var title = System.Web.HttpContext.Current.Request.Params["title"];
            var context = System.Web.HttpContext.Current.Request.Params["context"];
            var lblIdList = System.Web.HttpContext.Current.Request.Params["lblId"];

            IList<Lable> lblList = new List<Lable>();
            if (lblIdList != null && !string.IsNullOrEmpty(lblIdList))
            {
                foreach (var lbl in lblIdList.Split(',').Select(s => new Lable()
                    {
                        Id = s
                    }))
                {
                    lblList.Add(lbl);
                }
            }
            var article = new Article
                {
                    Id = id,
                    User = users,
                    Title = title,
                    Contents = context,
                    Lables = lblList
                };

            articleDal.Update(article);
            return Json(new
                {
                    IsSuccess = true,
                    JsExecuteMethod = "ajax_AddArticle"
                }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// /Manage/GetLableNameByUserIdAndArticleId
        /// </summary>
        /// <returns></returns>
        public ActionResult GetLableNameByUserIdAndArticleId(string articleid)
        {
            var user = System.Web.HttpContext.Current.Session["username"];
            if (user == null)
            {
                return Json(new { success = false, errors = new { msg = "login" } }, JsonRequestBehavior.AllowGet);
            }
            LableDal lableDal = new LableDal();
            var mydata = lableDal.MyGetAllByUserIdAndArticleId(user.ToString(), articleid);
            var alldata = lableDal.MyGetAllByUserId(user.ToString());
            var json = Json(new { success = true, data = new { alldata, mydata } }, JsonRequestBehavior.AllowGet);
            return json;
        }

        /// <summary>
        /// 删除文章和与之关联的评论和标签数据
        /// </summary>
        /// <returns></returns>
        public ActionResult DeleteArticle()
        {
            var articleDal = new ArticleDal();
            var id = System.Web.HttpContext.Current.Request.Params["articleid"];
            if (id != null)
            {
                try
                {
                    articleDal.Delete(id);
                    return Json(new { IsSuccess = true, JsExecuteMethod = "ajax_DeleteArticle" }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                    throw;
                }
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        //-------------------------------------------文章管理结束-------------------------------------------------------


        //-------------------------------------------标签管理开始-------------------------------------------------------

        public ActionResult LabelData(string start, string limit)
        {
            var labelDal = new LableDal();
            var user = System.Web.HttpContext.Current.Session["username"];
            if (user == null)
            {
                return Redirect("~/Logins/Login");
            }
            var total = 0;
            var data = labelDal.MyGetAllByUserName(user.ToString(), int.Parse(start), int.Parse(limit), ref total);
            var json = Json(new { totalCount = total, topics = data }, JsonRequestBehavior.AllowGet);
            return json;
        }

        /// <summary>
        /// 添加标签
        /// </summary>
        /// <returns></returns>
        public ActionResult AddLabel()
        {
            var name = System.Web.HttpContext.Current.Request["name"];
            var level = System.Web.HttpContext.Current.Request["level"];
            var labelDal = new LableDal();
            var userDal = new UserDal();

            var user = System.Web.HttpContext.Current.Session["username"];
            if (user == null)
            {
                return Json(new { success = false, errors = new { info = "请重新登录系统" } }, JsonRequestBehavior.AllowGet);
            }
            var users = userDal.GetByName(user.ToString());
            var userList = new List<User> { users };
            labelDal.Save(new Lable()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = name,
                    Level = int.Parse(level),
                    CreateTime = DateTime.Now,
                    Users = userList
                });
            var json2 = Json(new { success = true }, JsonRequestBehavior.AllowGet);
            return json2;
        }

        /// <summary>
        /// 编辑标签
        /// </summary>
        /// <returns></returns>
        public ActionResult EditLabel()
        {
            var id = System.Web.HttpContext.Current.Request["id"];
            var name = System.Web.HttpContext.Current.Request["name"];
            var level = System.Web.HttpContext.Current.Request["level"];
            var labelDal = new LableDal();
            var userDal = new UserDal();

            var user = System.Web.HttpContext.Current.Session["username"];
            if (user == null)
            {
                return Redirect("~/Logins/Login");
            }
            var users = userDal.GetByName(user.ToString());
            var userList = new List<User> { users };
            labelDal.Update(new Lable()
            {
                Id = id,
                Name = name,
                Level = int.Parse(level),
                Users = userList
            });
            return View("LabelManage");
        }

        /// <summary>
        /// 编辑标签名
        /// </summary>
        /// <returns></returns>
        public ActionResult EditLabelName()
        {
            var id = System.Web.HttpContext.Current.Request["id"];
            var name = System.Web.HttpContext.Current.Request["name"];
            var labelDal = new LableDal();
            var userDal = new UserDal();

            var user = System.Web.HttpContext.Current.Session["username"];
            if (user == null)
            {
                return Redirect("~/Logins/Login");
            }
            var users = userDal.GetByName(user.ToString());
            var userList = new List<User> { users };
            labelDal.UpdateName(new Lable()
            {
                Id = id,
                Name = name,
                Users = userList
            });
            return View("LabelManage");
        }

        /// <summary>
        /// 编辑标签等级
        /// </summary>
        /// <returns></returns>
        public ActionResult EditLabelLevel()
        {
            var id = System.Web.HttpContext.Current.Request["id"];
            var level = System.Web.HttpContext.Current.Request["level"];
            var labelDal = new LableDal();
            var userDal = new UserDal();

            var user = System.Web.HttpContext.Current.Session["username"];
            if (user == null)
            {
                return Redirect("~/Logins/Login");
            }
            var users = userDal.GetByName(user.ToString());
            var userList = new List<User> { users };
            labelDal.UpdateLevel(new Lable()
            {
                Id = id,
                Level = int.Parse(level),
                Users = userList
            });
            return View("LabelManage");
        }

        /// <summary>
        /// 删除标签
        /// </summary>
        /// <returns></returns>
        public ActionResult DeleteLabel()
        {
            var labelDal = new LableDal();
            var id = System.Web.HttpContext.Current.Request.Params["id"];
            var user = System.Web.HttpContext.Current.Session["username"];
            if (user == null)
            {
                return Redirect("~/Logins/Login");
            }

            if (id != null)
            {
                try
                {
                    labelDal.Delete(id);
                    return View("LabelManage");
                }
                catch (Exception)
                {
                    return View("LabelManage");
                    throw;
                }
            }
            return View("LabelManage");
        }
        //-------------------------------------------标签管理结束-------------------------------------------------------


        //-------------------------------------------用户管理开始-------------------------------------------------------

        /// <summary>
        /// 获取用户数据
        /// </summary>
        /// <param name="start"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public ActionResult UserData(string start, string limit)
        {
            var userDal = new UserDal();
            var user = System.Web.HttpContext.Current.Session["username"];
            if (user == null)
            {
                return Redirect("~/Logins/Login");
            }
            var total = 0;
            var data = userDal.MyGetAllByUserName(int.Parse(start), int.Parse(limit), ref total);
            var list = from user1 in data
                       select new
                           {
                               user1.Id,
                               user1.Name,
                               user1.PostTitle,
                               user1.Department,
                               user1.Avatar
                           };
            var json = Json(new { totalCount = total, topics = list }, JsonRequestBehavior.AllowGet);
            return json;
        }

        /// <summary>
        /// 添加用户
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddUser()
        {
            const string savePath = "/Images/Avatar/";
            const string saveUrl = "/Images/Avatar/";
            const string fileTypes = "gif,jpg,jpeg,png,bmp";
            const int maxSize = 1000000;

            Hashtable hash;

            HttpPostedFile file = System.Web.HttpContext.Current.Request.Files["upload"];
            var name = System.Web.HttpContext.Current.Request.Params["name"];
            var password = System.Web.HttpContext.Current.Request.Params["password"];
            var posttitle = System.Web.HttpContext.Current.Request.Params["posttitle"];
            var department = System.Web.HttpContext.Current.Request.Params["department"];
            if (file == null)
            {
                hash = new Hashtable();
                hash["error"] = 1;
                hash["message"] = "请选择文件";
                return Json(hash, "text/html;charset=UTF-8");
            }

            string dirPath = System.Web.HttpContext.Current.Server.MapPath(savePath);
            if (!Directory.Exists(dirPath))
            {
                hash = new Hashtable();
                hash["error"] = 1;
                hash["message"] = "上传目录不存在";
                return Json(hash, "text/html;charset=UTF-8");
            }

            string fileName = file.FileName;
            string fileExt = Path.GetExtension(fileName).ToLower();

            ArrayList fileTypeList = ArrayList.Adapter(fileTypes.Split(','));

            if (file.InputStream == null || file.InputStream.Length > maxSize)
            {
                hash = new Hashtable();
                hash["error"] = 1;
                hash["message"] = "上传文件大小超过限制";
                return Json(hash, "text/html;charset=UTF-8");
            }

            if (string.IsNullOrEmpty(fileExt) || Array.IndexOf(fileTypes.Split(','), fileExt.Substring(1).ToLower()) == -1)
            {
                hash = new Hashtable();
                hash["error"] = 1;
                hash["message"] = "上传文件扩展名是不允许的扩展名";
                return Json(hash, "text/html;charset=UTF-8");
            }

            string newFileName = DateTime.Now.ToString("yyyyMMddHHmmss_ffff", DateTimeFormatInfo.InvariantInfo) + fileExt;
            string filePath = dirPath + newFileName;
            file.SaveAs(filePath);
            string fileUrl = saveUrl + newFileName;

            hash = new Hashtable();
            try
            {
                var userDal = new UserDal();
                var blogDal = new BlogDal();
                var user = new User()
                    {
                        Name = name,
                        Password = password,
                        PostTitle = posttitle,
                        Department = department,
                        CreateTime = DateTime.Now,
                        Avatar = fileUrl
                    };
                var blog = new Blog { User = user, BlogCss = "../../Scripts/Lib/Blog/Blog.css" };

                user.Blog = blog;
                blogDal.Save(blog);
                userDal.Save(user);


                hash["success"] = true;
                hash["msg"] = "上传成功";
                return Json(hash, "text/html;charset=UTF-8");
            }
            catch (Exception)
            {
                hash["success"] = false;
                hash["msg"] = "上传失败";
                return Json(hash, "text/html;charset=UTF-8");
            }
        }

        /// <summary>
        /// 编辑用户
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditUser()
        {
            var userDal = new UserDal();
            const string savePath = "/Images/Avatar/";
            const string saveUrl = "/Images/Avatar/";
            const string fileTypes = "gif,jpg,jpeg,png,bmp";
            const int maxSize = 1000000;

            Hashtable hash;

            HttpPostedFile file = System.Web.HttpContext.Current.Request.Files["upload"];
            var id = System.Web.HttpContext.Current.Request.Params["id"];
            var posttitle = System.Web.HttpContext.Current.Request.Params["posttitle"];
            var department = System.Web.HttpContext.Current.Request.Params["department"];
            if (file == null)
            {
                hash = new Hashtable();
                hash["success"] = false;
                hash["msg"] = "请选择上传文件";
                return Json(hash, "text/html;charset=UTF-8");
            }

            string dirPath = System.Web.HttpContext.Current.Server.MapPath(savePath);
            if (!Directory.Exists(dirPath))
            {
                hash = new Hashtable();
                hash["success"] = false;
                hash["msg"] = "上传目录不存在";
                return Json(hash, "text/html;charset=UTF-8");
            }

            string fileName = file.FileName;
            string fileExt = Path.GetExtension(fileName).ToLower();

            ArrayList fileTypeList = ArrayList.Adapter(fileTypes.Split(','));

            if (file.InputStream == null || file.InputStream.Length > maxSize)
            {
                hash = new Hashtable();
                hash["success"] = false;
                hash["msg"] = "上传文件大小超过限制";
                return Json(hash, "text/html;charset=UTF-8");
            }

            if (string.IsNullOrEmpty(fileExt) || Array.IndexOf(fileTypes.Split(','), fileExt.Substring(1).ToLower()) == -1)
            {
                hash = new Hashtable();
                hash["success"] = false;
                hash["msg"] = "上传文件扩展名是不允许的扩展名";
                return Json(hash, "text/html;charset=UTF-8");
            }

            string newFileName = DateTime.Now.ToString("yyyyMMddHHmmss_ffff", DateTimeFormatInfo.InvariantInfo) + fileExt;
            string filePath = dirPath + newFileName;
            file.SaveAs(filePath);
            string fileUrl = saveUrl + newFileName;

            hash = new Hashtable();
            try
            {
                var user = userDal.GetByUser(id);
                var imgurl = user.Avatar;
                var pathall = _userimgpath + imgurl.Replace("/", "\\");
                if (System.IO.File.Exists(pathall))
                {
                    System.IO.File.Delete(pathall);
                }
                userDal.Update(new User()
                {
                    Id = id,
                    PostTitle = posttitle,
                    Department = department,
                    Avatar = fileUrl
                });
                hash["success"] = true;
                hash["msg"] = "上传成功";
                return Json(hash, "text/html;charset=UTF-8");
            }
            catch (Exception)
            {
                hash["success"] = false;
                hash["msg"] = "上传失败";
                return Json(hash, "text/html;charset=UTF-8");
            }
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <returns></returns>
        public ActionResult DeleteUser()
        {
            var userdal = new UserDal();
            var id = System.Web.HttpContext.Current.Request.Params["id"];
            if (id != null)
            {
                try
                {
                    var user = userdal.GetByUser(id);
                    var imgurl = user.Avatar;
                    var pathall = _userimgpath + imgurl.Replace("/", "\\");
                    if (System.IO.File.Exists(pathall))
                    {
                        System.IO.File.Delete(pathall);
                    }
                    userdal.Delete(id);
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                    throw;
                }
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        //-------------------------------------------用户管理结束-------------------------------------------------------


        //-------------------------------------------通知管理开始-------------------------------------------------------

        public ActionResult NoticeData(string start, string limit)
        {
            var youodal = new YouoDal();
            var user = System.Web.HttpContext.Current.Session["username"];
            if (user == null)
            {
                return Redirect("~/Logins/Login");
            }
            var total = 0;
            var data = youodal.MyGetAll(int.Parse(start), int.Parse(limit), ref total);
            var json = Json(new { totalCount = total, topics = data }, JsonRequestBehavior.AllowGet);
            return json;
        }

        /// <summary>
        /// 添加通知
        /// </summary>
        /// <returns></returns>
        public ActionResult AddNotice()
        {
            var content = System.Web.HttpContext.Current.Request["content"];
            var youoDal = new YouoDal();

            youoDal.Save(new Youo()
            {
                Id = Guid.NewGuid().ToString(),
                Contents = content,
                CreateTime = DateTime.Now
            });
            var json2 = Json(new { success = true }, JsonRequestBehavior.AllowGet);
            return json2;
        }

        /// <summary>
        /// 编辑通知
        /// </summary>
        /// <returns></returns>
        public ActionResult EditNotice()
        {
            var id = System.Web.HttpContext.Current.Request["id"];
            var content = System.Web.HttpContext.Current.Request["content"];

            var youoDal = new YouoDal();
            youoDal.Update(new Youo()
            {
                Id = id,
                Contents = content
            });
            var json2 = Json(new { success = true }, JsonRequestBehavior.AllowGet);
            return json2;
        }

        /// <summary>
        /// 删除通知
        /// </summary>
        /// <returns></returns>
        public ActionResult DeleteNotice()
        {
            var youoDal = new YouoDal();
            var id = System.Web.HttpContext.Current.Request.Params["id"];
            if (id != null)
            {
                try
                {
                    youoDal.Delete(id);
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                    throw;
                }
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        //-------------------------------------------通知管理结束-------------------------------------------------------

        //-------------------------------------------博客管理(又可分为：标题设置，模板设置和联系方式设置)开始-------------------------------------------------------
        /// <summary>
        /// 获取标题数据
        /// </summary>
        /// <returns></returns>
        public ActionResult TitleData()
        {
            var blogDal = new BlogDal();
            var userDal = new UserDal();
            var username = System.Web.HttpContext.Current.Session["username"];
            if (username == null)
            {
                return Redirect("~/Logins/Login");
            }
            var user = userDal.GetByName(username.ToString());
            var data = blogDal.GetById(user.Id);
            var json = Json(new
            {
                id = data.Id,
                bigTitle = data.BigTitle,
                smallTitle = data.SmallTitle,
                blogCss = data.BlogCss
            }, JsonRequestBehavior.AllowGet);
            return json;
        }

        /// <summary>
        /// 添加标题
        /// </summary>
        /// <returns></returns>
        public ActionResult AddTitle()
        {
            var blogDal = new BlogDal();
            var id = System.Web.HttpContext.Current.Request["id"];
            var bigTitle = System.Web.HttpContext.Current.Request["bigTitle"];
            var smallTitle = System.Web.HttpContext.Current.Request["smallTitle"];
            var template = System.Web.HttpContext.Current.Request["template"];
            var username = System.Web.HttpContext.Current.Session["username"];
            if (username == null)
            {
                return Redirect("~/Logins/Login");
            }
            var blog = new Blog()
                {
                    Id = id,
                    BigTitle = bigTitle,
                    SmallTitle = smallTitle,
                    BlogCss = template
                };
            blogDal.Update(blog);
            return Redirect("~/BlogManage/BlogSet");
        }

        public ActionResult UpdateKey()
        {
            var userDal = new UserDal();
            var keyold = System.Web.HttpContext.Current.Request["keyold"];
            var keynew1 = System.Web.HttpContext.Current.Request["keynew1"];
            var keynew2 = System.Web.HttpContext.Current.Request["keynew2"];

            var username = System.Web.HttpContext.Current.Session["username"];
            if (username == null)
            {
                return Redirect("~/Logins/Login");
            }
            var user = userDal.GetByName(username.ToString());
            if (keyold != user.Password)
            {
                return Json(new { success = false, msg = "原密码错误" }, JsonRequestBehavior.AllowGet);
            }
            user.Password = keynew1;
            userDal.Update(user);
            var json2 = Json(new { success = true, msg = "修改成功" }, JsonRequestBehavior.AllowGet);
            System.Web.HttpContext.Current.Session.Abandon();
            return json2;
        }

        //-------------------------------------------博客管理结束-------------------------------------------------------

    }
}
