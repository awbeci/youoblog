﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using MyMvc4Project.Models;
using NHibernate;
using System.Web.Mvc;
using MyMvc4Project.Infrastructure.Helpers;
using MyMvc4Project.Dal.Implementation;
using MyMvc4Project.Service;

namespace MyMvc4Project.Controllers
{
    public class LoginsController : Controller
    {
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public JsonResult UserLogin(string username, string password)
        {
            var userDal = new UserDal();
            var data = userDal.GetUserBy(username, password);
            var zwJson = new ZwJson();
            if (data == null)
            {
                zwJson.IsSuccess = false;
                zwJson.Msg = "用户名或密码错误，请确认后再输入！";
            }
            else
            {
                zwJson.IsSuccess = true;
                zwJson.Other = data.Name;
                zwJson.JsExecuteMethod = "ajax_UserLogin";
                System.Web.HttpContext.Current.Session["username"] = username;
                var remeber = System.Web.HttpContext.Current.Request.Params["remeber-me"];
                if (remeber != null)
                {
                    System.Web.HttpContext.Current.Session.Timeout = 15 * 24 * 60;
                }

            }
            var json = Json(zwJson, JsonRequestBehavior.AllowGet);
            return json;
        }

        public ActionResult Register()
        {
            return View();
        }

        public ActionResult AddUser(string username, string password, string remeberme)
        {
            var userDal = new UserDal();
            var blogDal = new BlogDal();
            var user = new User()
            {
                CreateTime = DateTime.Now,
                Name = username,
                Password = password,
                Department = "开发部",
                PostTitle = "程序员",
                Avatar = "/Images/people.jpg"
            };
            var blog = new Blog {  User = user, BlogCss = "../../Scripts/Lib/Blog/Blog.css" };

            user.Blog = blog;
            blogDal.Save(blog);
            userDal.Save(user);
            System.Web.HttpContext.Current.Session["username"] = username;
            if (remeberme != null)
            {
                System.Web.HttpContext.Current.Session.Timeout = 15 * 24 * 60;
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Logout()
        {
            System.Web.HttpContext.Current.Session.Abandon();
            return Redirect("~/Logins/Login");
        }
    }
}
