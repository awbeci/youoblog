﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using MyMvc4Project.Dal.Views;
using MyMvc4Project.Models;
using NHibernate;
using System.Web.Mvc;
using MyMvc4Project.Infrastructure.Helpers;
using MyMvc4Project.Dal.Implementation;
using MyMvc4Project.Service;

namespace MyMvc4Project.Controllers
{
    public class BlogController : Controller
    {
        readonly ISession _session = SessionHelper.GetCurrentSession();
        private static string _username;

        /// <summary>
        /// Blog/Index
        /// Blog/Index/id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Index(string id)
        {
            var userDal = new UserDal();
            if (string.IsNullOrEmpty(id))
            {
                return View("Error");
            }
            _username = id;
            var data = userDal.GetUserBy(_username);
            ViewData["Data"] = data;
            return data == null ? View("Error") : View();
        }

        /// <summary>
        /// /Blog/Article
        /// </summary>
        /// <returns></returns>
        public ActionResult Article(string name, string id)
        {
            ViewData["sessionname"] = GetSession();
            _username = name;
            var userDal = new UserDal();
            var articleDal = new ArticleDal();
            var commentDal = new CommentDal();

            var commentdata = commentDal.FindByComment(id);
            ViewData["comment"] = commentdata;
            if (string.IsNullOrEmpty(id))
            {
                return View("Error");
            }
            articleDal.UpdateClickCount(new Article { Id = id });
            var data = articleDal.GetById(id, _username);
            ViewData["Data"] = userDal.GetUserBy(_username);
            return data == null ? View("Error") : View(data);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="para"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public ActionResult ArticleList(string name, string para, string type)
        {
            _username = name;
            var userDal = new UserDal();
            var articleDal = new ArticleDal();
            IEnumerable<Article> data;
            switch (type)
            {
                case "lb":
                    data = articleDal.GetByClause(name, para);
                    break;
                case "dt":
                    var datetime = Convert.ToDateTime(para);
                    var datetime2 = datetime.AddMonths(1);
                    data = articleDal.GetByClause(name, datetime, datetime2);
                    break;
                default:
                    return View("Error");
            }
            ViewData["Data"] = userDal.GetUserBy(_username);
            return View(data);
        }

        public string GetSession()
        {
            var name = System.Web.HttpContext.Current.Session["username"];
            return name == null ? "" : name.ToString();
        }

        /// <summary>
        /// 获取文章
        /// </summary>
        /// <param name="currentPage"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetAllArticle(int currentPage)
        {
            //var name = GetSession();
            const int totalPages = 10;//每页多少数据
            var articleDal = new ArticleDal();
            var count = 0;
            var data = articleDal.MyGetAll(_username, currentPage, totalPages, ref count);
            var enumerable = data as Article[] ?? data.ToArray();
            var list = from article in enumerable
                       select new
                           {
                               article.Id,
                               UserName = article.User.Name,
                               article.Title,
                               article.CreateTime,
                               article.Contents,
                               Lables = from lbl in article.Lables
                                        select new
                                            {
                                                lbl.Name
                                            }
                           };
            var zwJson = new ZwJson
            {
                Data = list,
                Other = count.ToString(),
                IsSuccess = true,
                JsExecuteMethod = "ajax_GetAllArticle"
            };
            var json = Json(zwJson, JsonRequestBehavior.AllowGet);
            return json;
        }

        /// <summary>
        /// 获取标签
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult MyGetAllByUserId()
        {
            LableDal lableDal = new LableDal();
            var data = lableDal.MyGetAllByUserId(_username);
            var zwJson = new ZwJson
            {
                Data = data,
                Other = _username,
                IsSuccess = true,
                JsExecuteMethod = "ajax_GetUserLable"
            };
            var json = Json(zwJson, JsonRequestBehavior.AllowGet);
            return json;
        }

        /// <summary>
        /// 获取档案
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetArchives()
        {
            ArticleDal articleDal = new ArticleDal();
            var data = articleDal.GetBySql(_username);
            var zwJson = new ZwJson
            {
                Data = data,
                Other = _username,
                IsSuccess = true,
                JsExecuteMethod = "ajax_GetArchives"
            };
            var json = Json(zwJson, JsonRequestBehavior.AllowGet);
            return json;
        }

        [HttpPost]
        public JsonResult LoginUserMainPage()
        {
            var name = GetSession();
            ZwJson zwJson = new ZwJson();
            if (string.IsNullOrEmpty(name))
            {
                zwJson.Other = "/Logins/Login";
            }
            else
            {
                zwJson.Other = "/Blog/Index/" + name;
            }
            zwJson.JsExecuteMethod = "ajax_LoginUserMainPage";
            zwJson.IsSuccess = true;
            var json = Json(zwJson, JsonRequestBehavior.AllowGet);
            return json;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="contents"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SubmitComment(string name, string contents)
        {
            UserDal userDal = new UserDal();
            string data = System.Web.HttpContext.Current.Request.Url.Query;
            ZwJson zwJson = new ZwJson();
            var user = userDal.GetByName(name);
            if (user == null)
            {
                zwJson.IsSuccess = false;
                zwJson.Msg = "用户名错误！";
                return Json(zwJson, JsonRequestBehavior.AllowGet);
            }
            CommentDal commentDal = new CommentDal();
            try
            {
                commentDal.Save(new Comment()
                {
                    Id = Guid.NewGuid().ToString(),
                    Article = new Article()
                        {
                            Id = data.Split('?')[1]
                        },
                    User = user,
                    Contents = contents,
                    CommentTime = DateTime.Now
                });
            }
            catch (Exception)
            {
                throw;
            }
            zwJson.IsSuccess = true;
            zwJson.JsExecuteMethod = "ajax_SubmitComment";
            var json = Json(zwJson, JsonRequestBehavior.AllowGet);
            return json;
        }
    }
}
