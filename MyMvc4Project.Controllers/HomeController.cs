﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml;
using System.Web.Mvc;
using NHibernate;
using MyMvc4Project.Infrastructure.Helpers;
using MyMvc4Project.Infrastructure.Rss;
using MyMvc4Project.Dal.Implementation;
using MyMvc4Project.Service;

namespace MyMvc4Project.Controllers
{
    public class HomeController : Controller
    {
        readonly string _cnblogRssPath = ConfigurationManager.AppSettings["cnblog-news"];
        /// <summary>
        /// 获取所有文章
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetAllArticle(int currentPage)
        {
            const int totalPages = 20;//每页多少数据
            var articleDal = new ArticleDal();
            var count = 0;
            var data = articleDal.MyGetAll(currentPage, totalPages, ref count);
            var list = from article in data
                       select new
                           {
                               article.Id,
                               article.CreateTime,
                               article.Title,
                               article.Contents,
                               UserPostTitle = article.User.PostTitle,
                               UserAvatar = article.User.Avatar,
                               article.User.Department,
                               UserName = article.User.Name,
                               Lables = from label in article.Lables
                                        select new
                                            {
                                                label.Name
                                            }
                           };
            var zwJson = new ZwJson
            {
                Data = list,
                Other = count.ToString(),
                IsSuccess = true,
                JsExecuteMethod = "ajax_GetAllArticle"
            };
            var json = Json(zwJson, JsonRequestBehavior.AllowGet);
            return json;
        }

        /// <summary>
        /// 获取所有标签
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetAllLable()
        {
            LableDal lableDal = new LableDal();
            var data = lableDal.MyGetAll();
            var list = from lable in data
                       select new
                           {
                               lable.Name,
                               lable.Level
                           };
            var zwJson = new ZwJson
            {
                Data = from view in data
                       select new
                           {
                               view.Id,
                               view.Name,
                               view.Level
                           },
                IsSuccess = true,
                JsExecuteMethod = "ajax_GetAllLable"
            };
            var json = Json(zwJson, JsonRequestBehavior.AllowGet);
            return json;
        }

        /// <summary>
        /// 获取优尔数据
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetYouo()
        {
            YouoDal youoDal = new YouoDal();
            var data = youoDal.MyGetFirst();
            var zwJson = new ZwJson
            {
                FirstData = data,
                IsSuccess = true,
                JsExecuteMethod = "ajax_GetYouo"
            };
            var json = Json(zwJson, JsonRequestBehavior.AllowGet);
            return json;
        }

        public ActionResult Index()
        {
            var name = System.Web.HttpContext.Current.Session["username"];
            ViewData["name"] = name;
            return View();
        }

        public ActionResult About()
        {

            return View();
        }

        /// <summary>
        /// 获取博客园新闻Rss数据
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetRssXmlDoc()
        {
            XmlDocument doc = new XmlDocument();
            IList<RssXml> rssXmls = new List<RssXml>();
            try
            {
                //加载Xml文件  
                doc.Load(_cnblogRssPath);
                //根结点
                XmlNode dataTableSettingsNode = doc.SelectSingleNode("rss");
                if (dataTableSettingsNode != null)
                {
                    XmlNode dataCellsNode = dataTableSettingsNode.SelectSingleNode("channel");

                    if (dataCellsNode != null)
                    {
                        XmlNodeList dataCellNode = dataCellsNode.SelectNodes("item");

                        if (dataCellNode != null)
                            foreach (XmlNode node in dataCellNode)
                            {
                                RssXml rssXml = new RssXml();
                                var title = node.SelectSingleNode("title");
                                var link = node.SelectSingleNode("link");
                                var pubdate = node.SelectSingleNode("pubdate");
                                var guid = node.SelectSingleNode("guid");
                                var description = node.SelectSingleNode("description");
                                if (title != null)
                                    rssXml.Title = title.InnerText;
                                if (link != null)
                                    rssXml.Link = link.InnerText;
                                if (pubdate != null)
                                    rssXml.PubDate = pubdate.InnerText;
                                if (guid != null)
                                    rssXml.Guid = guid.InnerText;
                                if (description != null)
                                    rssXml.Description = description.InnerText;
                                rssXmls.Add(rssXml);
                            }
                    }
                }
                var zwJson = new ZwJson
                {
                    Data = rssXmls,
                    IsSuccess = true,
                    JsExecuteMethod = "ajax_GetRssXmlDoc"
                };
                var json = Json(zwJson, JsonRequestBehavior.AllowGet);
                return json;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 获取所有用户信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetAllUser()
        {
            var userDal = new UserDal();
            var data = userDal.GetAll1();
            var list = from user in data
                       select new
                           {
                               user.Name,
                               user.Avatar
                           };
            var zwjson = new ZwJson
                {
                    Data = list,
                    IsSuccess = true,
                    JsExecuteMethod = "ajax_GetAllUser"
                };
            var json = Json(zwjson, JsonRequestBehavior.AllowGet);
            return json;
        }

        /// <summary>
        /// 用户退出登入
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Quit()
        {
            System.Web.HttpContext.Current.Session.Remove("username");
            ZwJson zwJson = new ZwJson()
                {
                    IsSuccess = true,
                    JsExecuteMethod = "ajax_Quit"
                };
            var json = Json(zwJson, JsonRequestBehavior.AllowGet);
            return json;
        }
    }
}
