﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using MyMvc4Project.Dal.Views;
using MyMvc4Project.Models;
using NHibernate;
using System.Web.Mvc;
using MyMvc4Project.Infrastructure.Helpers;
using MyMvc4Project.Dal.Implementation;
using MyMvc4Project.Service;

namespace MyMvc4Project.Controllers
{
    public class BlogManageController : Controller
    {
        /// <summary>
        /// /BlogManage/Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var user = System.Web.HttpContext.Current.Session["username"];
            if (user == null)
            {
                return Redirect("~/Logins/Login");
            }
            return View();
        }

        /// <summary>
        /// /BlogManage/NewArticle
        /// </summary>
        /// <returns></returns>
        public ActionResult NewArticle()
        {
            var user = System.Web.HttpContext.Current.Session["username"];
            if (user == null)
            {
                return Redirect("~/Logins/Login");
            }
            LableDal lableDal = new LableDal();
            var data = lableDal.MyGetAllByUserId(user.ToString());
            ViewData["lable"] = data;
            return View();
        }

        /// <summary>
        /// /BlogManage/ArticleManage
        /// </summary>
        /// <returns></returns>
        public ActionResult ArticleManage()
        {
            var action = System.Web.HttpContext.Current.Request.Params["action"];
            var user = System.Web.HttpContext.Current.Session["username"];
            if (user == null)
            {
                return Redirect("~/Logins/Login");
            }
            ViewData["action"] = action;
            return View();
        }

        /// <summary>
        /// /BlogManage/LabelManage
        /// </summary>
        /// <returns></returns>
        public ActionResult LabelManage()
        {
            var action = System.Web.HttpContext.Current.Request.Params["action"];
            var user = System.Web.HttpContext.Current.Session["username"];
            if (user == null)
            {
                return Redirect("~/Logins/Login");
            }
            ViewData["action"] = action;
            return View();
        }

        /// <summary>
        /// /BlogManage/KeyManage
        /// </summary>
        /// <returns></returns>
        public ActionResult KeyManage()
        {
            var user = System.Web.HttpContext.Current.Session["username"];
            if (user == null)
            {
                return Redirect("~/Logins/Login");
            }
            return View();
        }

        /// <summary>
        /// /BlogManage/BlogSet
        /// </summary>
        /// <returns></returns>
        public ActionResult BlogSet()
        {
            var user = System.Web.HttpContext.Current.Session["username"];
            if (user == null)
            {
                return Redirect("~/Logins/Login");
            }
            return View();
        }

        /// <summary>
        /// /BlogManage/EditArticle
        /// </summary>
        /// <returns></returns>
        public ActionResult EditArticle(string id)
        {
            var user = System.Web.HttpContext.Current.Session["username"];
            if (user == null)
            {
                return Redirect("~/Logins/Login");
            }
            var articleDal = new ArticleDal();
            var lblDal = new LableDal();
            try
            {
                var data = articleDal.GetById2(id, user.ToString());
                var lblData = lblDal.MyGetAllByUserId2(user.ToString());
                ViewData["article"] = data;
                ViewData["label"] = lblData;
            }
            catch (Exception e)
            {
                return Redirect("~/Home");
            }
            return View();
        }

        /// <summary>
        /// 获取文章数据
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public JsonResult GetArticleData(jQueryDataTableParamModel param)
        {
            var articleDal = new ArticleDal();
            var user = System.Web.HttpContext.Current.Session["username"];
            if (user == null)
            {
                return null;
            }
            var total = 0;
            var data = articleDal.MyGetAll3(user.ToString(), param.iDisplayStart, param.iDisplayLength, ref total);
            var json = Json(new { param.sEcho, iTotalDisplayRecords = total, iTotalRecords = total, aaData = data }, JsonRequestBehavior.AllowGet);
            return json;
        }

        /// <summary>
        /// 获取标签数据
        /// </summary>
        /// <returns></returns>
        public JsonResult GetLabelData(jQueryDataTableParamModel param)
        {
            var labelDal = new LableDal();
            var user = System.Web.HttpContext.Current.Session["username"];
            if (user == null)
            {
                return null;
            }
            var data = labelDal.GetAllByUserName(user.ToString());
            var list = from lable in data
                       select new
                           {
                               lable.Id,
                               lable.Name,
                               lable.Level,
                               lable.Articles.Count
                           };
            var json = Json(new { param.sEcho, aaData = list }, JsonRequestBehavior.AllowGet);
            return json;
        }
    }
}
