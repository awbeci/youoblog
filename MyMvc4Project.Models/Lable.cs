﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyMvc4Project.Models
{
    /// <summary>
    /// t_Lable:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public class Lable
    {
        /// <summary>
        /// id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 标签名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 显示等级
        /// </summary>
        public int Level { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { get; set; }

        /// <summary>
        /// 文章
        /// </summary>
        public IList<Article> Articles { get; set; }

        public IList<User> Users { get; set; }
    }
}

