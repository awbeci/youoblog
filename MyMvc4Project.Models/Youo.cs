﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyMvc4Project.Models
{
    public class Youo
    {
        /// <summary>
        /// id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public string Contents { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { get; set; }
    }
}
