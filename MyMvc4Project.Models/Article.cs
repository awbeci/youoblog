﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyMvc4Project.Models
{
    /// <summary>
    /// t_Article:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public class Article
    {
        /// <summary>
        /// id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 用户
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public string Contents { get; set; }

        /// <summary>
        /// 点击浏览次数
        /// </summary>
        public int ClickCount { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }

        public IList<Comment> Comments { get; set; }

        public IList<Lable> Lables { get; set; }
    }
}

