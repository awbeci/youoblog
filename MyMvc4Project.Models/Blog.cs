﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyMvc4Project.Models
{
    public class Blog
    {
        /// <summary>
        /// id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 小标题
        /// </summary>
        public string SmallTitle { get; set; }

        /// <summary>
        /// 大标题
        /// </summary>
        public string BigTitle { get; set; }

        /// <summary>
        /// 博客css
        /// </summary>
        public string BlogCss { get; set; }

        public User User { get; set; }
    }
}
