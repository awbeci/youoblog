﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyMvc4Project.Models
{
    /// <summary>
    /// t_User:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public class User
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 员工职称
        /// </summary>
        public string PostTitle { get; set; }

        /// <summary>
        /// 所属部门
        /// </summary>
        public string Department { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { get; set; }

        public IList<Article> Articles { get; set; }

        public IList<Comment> Comments { get; set; }

        public IList<Lable> Labels { get; set; }

        public Blog Blog { get; set; }
    }
}