﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyMvc4Project.Models
{
    /// <summary>
    /// t_Comment:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public class Comment
    {
        /// <summary>
        /// id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 文章
        /// </summary>
        public Article Article { get; set; }

        /// <summary>
        /// 被评论用户
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public string Contents { get; set; }

        /// <summary>
        /// 评论时间
        /// </summary>
        public DateTime? CommentTime { get; set; }
    }
}

